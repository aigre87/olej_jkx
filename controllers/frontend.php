<?
class frontend extends main
 { 	 
	 protected $user_data;
	 protected $organization_data;
	 
	 function __construct()
	  {		  		 
		 session_start();
		 $this->_init();
		 $this->_auth_check();
		 $this->_data_load();
		 $this->_router();
	  }
	 
	 function _data_load()
	  {
		  if(!isset($_SESSION['user_id']))
		   {
			   return false;
		   }
		   
		  $this->user_data 		   = $this->model->user_data_get($_SESSION['user_id']);
		  $this->organization_data = $this->model->organization_data_get($this->user_data['organization_id']);
	  }
	 
	 function clear()
	  {
		  $this->model->clear();
		  echo 'данные очищены';
	  }
	 
	 function per_day()
	  {
		  $this->model->lists_created_per_day();
	  }
	  
	 function scan_upload()
	  {
		  // print_r($_POST);
		  // print_r($_FILES); exit();
		  
		  $dir_path = 'data/scans';
		  $dir = scandir($dir_path);
		  unset($dir[0], $dir[1]);
		  
		  sort($dir, SORT_NUMERIC);
		  
		  if(empty($dir))
		   {
			   $file_id = 1;
		   }
		  else
		   {
			   $file_id = end($dir) + 1;
		   }			  
          
		  // print_r($dir); exit(); 
		   
		  $ext = explode('.', $_FILES['scan']['name']);
		  $ext = end($ext);
		  $file_path = $dir_path . '/' . $file_id . '.' . $ext;
		  $file_name = $file_id . '.' . $ext;
		  move_uploaded_file($_FILES['scan']['tmp_name'], $file_path);
		  
		  if(!empty($_POST['checklist_id']))
		   {
			  $this->model->file_assign($_POST['checklist_id'], $_POST['checklist_number'], $file_name);
		   }

		  
		  $files[] = ['name' => $file_name, 'size' => 9991];
		  echo( json_encode($files) );
	  }
	 
	 function workers_get()
	  {
		  echo json_encode($this->model->workers_get($_POST['brigada_id']));  
	  }	  
	 
	 function napravlenie_search()
	  {
		  // print_r($_POST); exit();
		  // echo json_encode($this->model->napravlenie_search($_POST['request'], $_POST['category_id'], $_POST['search_inn']));
		  $_POST['request'] = htmlspecialchars_decode($_POST['request']);
		  echo json_encode($this->model->napravlenie_search($_POST['request'], $_POST['category_id']));
	  }
	 
	 function questions_get()
	  {
		  // print_r($_POST);
		  echo json_encode($this->model->questions_get($_POST['category_id'], $_POST['checklist_number']));
	  }	 
	  
	 function place_parts_get()
	  {
		  $place_parts = $this->model->place_parts_get($_POST['work_type'], $_POST['place_type'], $_POST['check_list']);
		  echo json_encode($place_parts);
	  }
	  
	 function work_types_get()
	  {
		  echo json_encode($this->model->work_types_get($_POST['work_type_id']));
	  }
	
	 function napravlenie_get()
	  {
		  // echo $_POST['category_id']; exit();
		  
		  echo json_encode($this->model->napravlenie_cat_get_all($_POST['category_id']));
	  }
	 
	 function list_view()
	  {
		   if(empty($_GET['list_id']))
		    {
				exit('Не заданы параметры');
			}
		   
		   $list_number = (!empty($_GET['page']))? $_GET['page'] : 1;		   
		   
		   if(!($list_number >= 1 && $list_number <= 3))
		    {
				exit('некорректно задан номер страницы');
			}
		 
           $data = $this->model->list_view_data_build($_GET['list_id'], $list_number, $this->organization_data);		 
		   
		   if($data['status'] == 0)
		    {
				exit($data['error']);
			}
		   
		   $this->_show('frontend/list_view', $data);
		   
	  }
	 
	 function list_edit()
	  {
		  if(empty($_POST))
		   {
			   exit('не заданы параметры');
		   }

		  $_POST['organization_id']   = $this->organization_data['id'];

		  $result = $this->model->list_edit($_POST);
		  echo json_encode($result);		  
	  }
	 
	 function list_save_new()
	  {
		  if(empty($_POST))
		   {
			   exit('не заданы параметры');
		   }
		  
		  $_POST['creator_id']	    = $_SESSION['user_id'];
		  $_POST['organization_id'] = $this->organization_data['id'];
		  
		  // print_r($_POST); exit();
		  
		  $result = $this->model->list_create($_POST);
		  echo json_encode($result);
	  }
	 
	 function list_sign()
	  {
		  $result = $this->model->list_sign($_POST['checklist_id'], $_POST['checklist_number']);
		  echo json_encode($result);
	  }
	 
	 function list_create()
	  {
		  $data['organization'] = $this->organization_data;	
		  $data['brigadu']      = $this->model->brigadu_get($data['organization']['id']);
		  $data['napravlenie_list'] = $this->model->napravlenie_cat_get_all();
		  $data['work_types']   = $this->model->work_types_get();
		
		  // print_r($data['napravlenie_list']);
		  $this->_show('frontend/list_create', $data);
	  }
	 
	 function main()
	  {         		 	 		 
		 $show_all = (!empty($_GET['show_all']))? 1 : 0;
		
		 switch($this->user_data['access_level'])
		  {
			case 1:
				$data['checklists'] = $this->model->checklists_get_my($this->user_data['id'], $show_all);
			break;
			
			case 2:
				$data['checklists'] = $this->model->checklists_get_by_org($this->user_data['organization_available'], $show_all);
			break;
			
			case 3:
				$data['checklists'] = $this->model->checklists_get_all($show_all);
			break;
		  }
		 $this->_show('frontend/main', $data);
	  }	

	 function logout()
	  {
		  unset($_SESSION['user_id']);
		  $this->_show('frontend/auth');
	  }
	 
	 function _auth_check()
	  {
		  if(!isset($_SESSION['user_id'])) 
		   {
			   if(empty($_POST))
			    {
					$this->_show('frontend/auth');
					exit();
				}
			   else
			    {
					$user_data = $this->model->check_auth($_POST['login'], $_POST['password']);
					
					if($user_data)
					 {
						 $_SESSION['user_id'] = $user_data['id'];
					 }
					else
					 {
						 $this->_show('frontend/auth');
						 exit();						 
					 }						
				} 
		   }
	  }	 
 }

?>