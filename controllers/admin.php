<?
class admin extends main
 {	 
	 function __construct()
	  {		  
		  session_start();
		  $this->_init();		  
		  $this->_auth_check();
		  $this->_router();
	  }
	 
	 function main()
	  {
		  $this->_show('admin/main');
	  }
	  
	 function _auth_check()
	  {
		  if(!isset($_SESSION['admin']))
		   {
			   if(!empty($_POST))
			    {
					// проверяем пароль
					if($this->model->check_auth($_POST['login'], $_POST['password']))
					 {
						 $_SESSION['login'] = $_POST['login'];
					 }
					else
					 {
						 $this->_show('admin/login');
						 exit();						 
					 }
				}
			   else
			    {
				   $this->_show('admin/login');
				   exit();
				}
		   }
	  }	 
 }
?>