<?
class main
 {
	 protected $model;
	 protected $config;
	 
	 // function __construct()
	 function _init()
	  {
		  require_once 'model/model.php';
		  $this->model = new model;
		  // $this->_config_load();
		  // $this->_router();
	  }
	 
	 function _config_load()
	  {
		  require 'config.php';
		  $this->config = $config;
	  }
	
	 function _router()
	  {
		  if(!empty($_GET['action']))
		   {
			   if(method_exists($this, $_GET['action']))
			    {
					if($_GET['action']{0} != '_')
					 {
						 $this->$_GET['action']();
					 }
					else
					 {
						 exit('403');
					 }						
				}
			   else
			    {
					exit('404');
				}				   
		   }
		  else
		   {
			   $this->main();
		   }
	  }
	  
	 function _show($_template, $data=array())
	  {		  
		  if(!is_array($data))
		   {
			   echo 'warning: $data is not array';
			   $data = array();
		   }
		  
		  unset($data['_template']);
		  extract($data);
		  
		  $_template = 'views/' . $_template . '.php';
		  if(!file_exists($_template))
		   {
			   exit("Ошибка: шаблон $_template не найден");
		   }
		  require($_template);
	  }
 }
?>