<html>
<body>

<script type="text/javascript" src="/views/frontend/js/jquery-2.2.2.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="/views/frontend/js/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/views/frontend/js/jquery.suggestions.min.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/views/frontend/js/datepicker-ru.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="/views/frontend/js/main.js"></script>


<link rel="stylesheet" href="/views/frontend/css/jquery-ui.css"/> 
<link href="/views/frontend/css/suggestions.min.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="/views/frontend/css/jquery.fileupload.css">

<style>
 select.place_parts, select#place_parts_dist
  {
	  display: none;
  }
  
 tr#workers_tr
  {
	  display: none;
  } 
  
 a.place_parts_open
  {
	  text-decoration: none;
  } 
  
 a:visited.place_parts_open
  {
	  color: blue;
  }
  
 div.error
  {
	  color: red;
  }
  
 a.questions_delete
  {
	  color: red;
  }
</style>

<? include 'menu.php'; ?>

<h1>Создание обходного листа</h1>

<select id="place_parts_dist"><option value=""></option></select>

<input type="hidden" id="checklist_id" value="1">
<input type="hidden" id="checklist_number" value="1">

<form method="POST" id="checklist_create_form" enctype="multipart/form-data">

	<table id="checklist_create_table">
		<tr>
			<td>Организация:</td>
			<td><?=$organization['name']?></td>
		</tr>
		
		<tr>
			<td>Бригада:</td>
			<td>
				<select id="brigada_select" name="brigada">
					<option value=""></option>
					<? foreach($brigadu as $brigada): ?>
						<option value="<?=$brigada['number']?>"><?=$brigada['number']?></option>
					<? endforeach; ?>
				</select>
			</td>
			
			<td>
				<div id="brigada_error" class="error"></div>
			</td>
		</tr>
		
		<tr>
			<td>Адрес:</td>
			<td>
				<input type="hidden" name="adress[index]">
				<input type="hidden" name="adress[city]">
				<input type="hidden" name="adress[street]">
				<input type="hidden" name="adress[rajon]">
				<input type="hidden" name="adress[okrug]">
				<input type="hidden" name="adress[block]">
				<input type="hidden" name="adress[dom]"> 
				<input type="hidden" name="adress[geocoord_lat]">
				<input type="hidden" name="adress[geocoord_lon]"> 
				<input id="adress" type="text" placeholder="Адрес" name="adress[raw]"> <br/>
				<input type="text" name="adress[appartment]" placeholder="офис/кабинет/квартира">
			</td>

			<td>
				<div id="adress_error" class="error"></div>
			</td>						
		</tr>
	
		<tr>
			<td>Направление: </td>
			<td id="napravlenie_select_td">
				<select id="napravlenie_select" name="napravlenie_parent_id">
					<option value=""></option>
					
					<? foreach($napravlenie_list as $napravlenie_category): ?>
						<option value="<?=$napravlenie_category['id']?>"><?=$napravlenie_category['name']?></option>
					<? endforeach; ?>
				</select>				
			</td>

			<input type="hidden" name="napravlenie_id" id="napravlenie_id">
				
			<td>
				<div id="napravlenie_error" class="error"></div>
			</td>			
			
		</tr>
		
		<tr>
			<td>Статус:</td>
			<td>Обследование</td>
		</tr>
		
		<tr id="vid_rabot_tr">
			<td>Вид работ: </td>
			<td id="vid_rabot_td">
				<select attr="work_type" name="work_type">
					<option value=""></option>
					
					<? foreach($work_types as $work_type): ?>
						<? 
							if(isset($_POST['work_type']))
						   	 {
								 $selected = ($_POST['work_type'] == $work_type['id'])? 'selected' : '';
							 }
							else $selected = '';
						?>
						
						<option <?=$selected?> value="<?=$work_type['id']?>"><?=$work_type['name']?></option>
					<? endforeach; ?> 
				</select>
			</td>
			
			<td>
				<div id="work_type_error" class="error"></div>
			</td>				
		</tr>
		
		<tr>
			<td>Дата: </td>
			<td>
				<input id="date_select" type="text" name="date">
			</td>
			
			<td>
				<div id="date_error" class="error"></div>
			</td>				
		</tr>
		
		<tr id="workers_tr">
			<td>Работники: </td>
			<td>
				<select id="workers_list" name="workers"> <!-- multiple -->
				</select>
			</td>
			
			<td>
				<div id="workers_error" class="error"></div>
			</td>				
		</tr>
		
		<!-- <input type="hidden" name="MAX_FILE_SIZE" value="30000" /> -->
		 
		<tr>
			<td>Загрузка скана</td>
			<td>
				<!--<input name="scan_upload" id="scan_upload" type="file" />-->
				<span class="btn btn-success fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Выберите файл...</span>
					<!-- The file input field used as target for the file upload widget -->
					<input id="scan_upload" type="file" name="scan"> <!-- multiple -->
				</span>
				
				<input type="hidden" name="scanlist" id="scanlist_name">
				
				<div id="files"></div>
				
				<div id="progress" class="progress">
					<div class="progress-bar progress-bar-success"></div>
				</div>		
			</td>
			<td>
				<div id="scan_upload_error" class="error"></div>
			</td>
		</tr> 
		
		
		<tr>
			<td align="center" colspan="2">
				<input type="submit" name="submit" id="list_create_button" value="Сохранить">
				<button onclick="window.print(); return false;">Печать</button>
			</td>
		</tr>
		
	</table>
	
</form>

</body>
</html>