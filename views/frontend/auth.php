
<!DOCTYPE html>
<html lang="en" class="auth">
<head>
	<meta charset="UTF-8">
	<title>Авторизация</title>
	<link rel="stylesheet" href="/front_build/css/template_styles.min.css">
	<script src="/front_build/libs/jquery.min.js"></script>
	<script src="/front_build/libs/jquery.magnific-popup.min.js"></script>
	<script src="/front_build/js/auth.js"></script>
</head>
<body>
	<div id="mainWrapper">
		<div class="authPage">
			<form class="loginForm" method="POST" action="/">
				<input type="text" name="login" placeholder="логин">
				<input type="password" name="password" placeholder="пароль">
				<input type="submit" value="Войти">
			</form>
		</div>
	</div>
</body>
</html>