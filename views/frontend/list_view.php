<html>
<body>

<script type="text/javascript" src="/views/frontend/js/jquery-2.2.2.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="/views/frontend/js/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/views/frontend/js/jquery.suggestions.min.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/views/frontend/js/datepicker-ru.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="/views/frontend/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="/views/frontend/js/main.js"></script>


<link rel="stylesheet" href="/views/frontend/css/jquery-ui.css"/> 
<link href="/views/frontend/css/suggestions.min.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/views/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="/views/frontend/css/jquery.fileupload.css">

<style>
 select.place_parts, select#place_parts_dist
  {
	  display: none;
  }
  
 tr#workers_tr
  {
	 // display: none;
  } 
  
 a.place_parts_open
  {
	  text-decoration: none;
  } 
  
 a:visited.place_parts_open
  {
	  color: blue;
  }
  
 div.error
  {
	  color: red;
  }
 
 div.list_numbers
  {
	  float: left;
	  margin-right: 0px;
	  margin-left: 0px;
  } 
  
 div.message
  {
	  color: green;
  } 
  
 table.questions_table.checklist_2  
  {
	  display: none;
  }
</style>

<? include 'menu.php'; ?>

<? // print_r($napravlenie_data); ?>

<div id="list_names">

	<? if($list_number == 1): ?>

		<div class="list_name checklist_1">
			<? if($this->user_data['access_level'] == 1 && (time() - strtotime($title_list['date'])) > 60*60*24*5): ?>
				<h1>Просмотр обходного листа</h1>
			<? else: ?>		
				<h1>Редактирование обходного листа</h1>	
			<? endif; ?>
		</div>

	<? elseif($list_number == 2): ?>
		
		<div class="list_name checklist_2">
			<? if($title_list['date_2'] != '00.00.0000'): ?>	
				<h1>Создание листа истребления</h1>
			<? else: ?>
				<h1>Редактирование листа истребления</h1>
			<? endif; ?>
		</div>

	<? elseif($list_number == 3): ?>
		
		<div class="list_name checklist_3">
			<? if($title_list['date_3'] != '00.00.0000'): ?>	
				<h1>Создание листа результатов</h1>
			<? else: ?>
				<h1>Редактирование листа результатов</h1>
			<? endif; ?>
		</div>
		
	<? endif; ?>

</div>

<? 
 // (time() - strtotime($title_list['date'])) > 60*60*24*5
?>

<div id="list_menu">
	
	<? if($data['list_number'] != 1): ?>
		<a id="list_1_edit_link" href="?action=list_view&list_id=<?=$title_list['id']?>&page=1">Редактировать обходной лист</a>
	<? endif; ?>

	<? if($title_list['date_2'] != '00.00.0000'): ?>
		
		<? if($data['list_number'] != 2): ?>
				<a style="display:<?=(!empty($title_list['number_1']))? 'block':'none'.';'?>" id="list_2_edit_link" href="?action=list_view&list_id=<?=$title_list['id']?>&page=2">Редактировать лист истребления</a>
		<? endif; ?>
		
		<? if($title_list['date_3'] != '00.00.0000'): ?>
			| <a style="display:<?=(!empty($title_list['number_2']))? 'block':'none'.';'?>" id="list_3_edit_link" href="?action=list_view&list_id=<?=$title_list['id']?>&page=3">Редактировать лист результатов</a>
		<? else: ?>
			| <a style="display:<?=(!empty($title_list['number_2']))? 'block':'none'.';'?>" id="list_3_create_link" href="?action=list_view&list_id=<?=$title_list['id']?>&page=3">Создать лист результатов</a>
		<? endif; ?>
		
	<? else: ?>

		<? if($data['list_number'] != 2): ?>
				<a style="display:<?=(!empty($title_list['number_1']))? 'block':'none'.';'?>" id="list_2_create_link" href="?action=list_view&list_id=<?=$title_list['id']?>&page=2">Создать лист истребления</a>
		<? endif; ?> 
		
	<? endif; ?>

</div>

<input type="hidden" id="checklist_2_number" value="<?=$title_list['number_2']?>">
<input type="hidden" id="checklist_3_number" value="<?=$title_list['number_3']?>">  

<form method="POST" id="checklist_edit_form" enctype="multipart/form-data">

	<input type="hidden" name="checklist_id" 	 id="checklist_id"     value="<?=$title_list['id']?>">
	<input type="hidden" name="checklist_number" id="checklist_number" value="<?=$list_number?>">
	
	<? if($title_list['date']): ?>
	
		<? if($this->user_data['access_level'] == 1 && (time() - strtotime($title_list['date'])) > 60*60*24*5): ?>
			<input type="hidden" id="disabled_edit" value="1">
		<? else: ?>		
			<input type="hidden" id="disabled_edit" value="0">		
		<? endif; ?>
		
	<? endif; ?>

	
	<? if($list_number > 1): ?>
		<input type="hidden" name="disabled_work_change" id="disabled_work_change" value="1">		
	<? else: ?>
		<input type="hidden" name="disabled_work_change" id="disabled_work_change" value="0">
	<? endif; ?>	
	
	
	<table id="checklist_edit_table">

		<tr>
			<td>Номер листа:</td>
			<td>
				<? if(!empty($title_list['number'])): ?>
					<?=$title_list['number']?>
				<? else: ?>
					<div>
						<? 
						  switch($work_type)
						   {
							   case 'Дизенсекция': $work_type = 'ДC'; break;
							   case 'Дизенфекция': $work_type = 'ДФ'; break;
							   case 'Дератизация': $work_type = 'ДР'; break;
							   case 'Акарицидная обработка': $work_type = 'АК'; break;			   
							   case 'Противомалярийная обработка': $work_type = 'МЛ'; break;			   
						   }				
						?>
						
						
						
						<div class="list_numbers" id="title_work_type"><?=$work_type?></div>
						<div class="list_numbers"><?=mb_strtoupper($organization_data['number'])?></div><div class="list_numbers">_</div>
						<div class="list_numbers" id="brigada_number"><?=$title_list['brigada']?></div><div class="list_numbers">_</div>
						<div class="list_numbers" id="date_number"><?=str_replace('.', '', $title_list['date'])?></div>
					</div>
				<? endif; ?>
			</td>
		</tr>		
		
		<tr>
			<td>Организация:</td>
			<td><?=$title_list['organization']?></td>
		</tr>
		
		<tr>
			<td>Бригада:</td>
			<td>
				<select id="brigada_select" name="brigada">
					<option value=""></option>
					<? foreach($brigadu as $brigada): ?>
						<option <?=($title_list['brigada'] == $brigada['number'])? 'selected' : ''?> value="<?=$brigada['number']?>"><?=$brigada['number']?></option>
					<? endforeach; ?>
				</select>
			</td>
			
			<td>
				<div id="brigada_error" class="error"></div>
			</td>
		</tr>
		
		<tr>
			<td>Адрес:</td>
			<td>
				<input type="hidden" value="<?=$title_list['adress_index']?>" name="adress[index]">
				<input type="hidden" value="<?=$title_list['adress_city']?>" name="adress[city]">
				<input type="hidden" value="<?=$title_list['adress_street']?>" name="adress[street]">
				<input type="hidden" value="<?=$title_list['adress_rajon']?>" name="adress[rajon]">
				<input type="hidden" value="<?=$title_list['adress_okrug']?>" name="adress[okrug]">
				<input type="hidden" value="<?=$title_list['adress_dom']?>" name="adress[dom]"> 
				<input type="hidden" value="<?=$title_list['adress_block']?>" name="adress[block]"> 
				<input type="hidden" value="<?=$title_list['adress_geocoord_lat']?>" name="adress[geocoord_lat]">
				<input type="hidden" value="<?=$title_list['adress_geocoord_lon']?>" name="adress[geocoord_lon]"> 
				<input id="adress" type="text" placeholder="Адрес" value="<?=$title_list['adress_raw']?>" name="adress[raw]"> <br/>
				<input type="text" value="<?=$title_list['adress_appartment']?>" name="adress[appartment]" placeholder="офис/кабинет/квартира">
			</td>

			<td>
				<div id="adress_error" class="error"></div>
			</td>						
		</tr>
	
		<tr>
			<td>Направление: </td>
			<td id="napravlenie_select_td">
				
				<? $count = 0; ?>
				
				<? foreach($napravlenie_data as $napravlenie_cat_all): ?>
					
					<? $count++; ?>
					
					<select <? if($count==1): ?> id="napravlenie_select" name="napravlenie_parent_id" <? endif; ?>>
						
						<option value=""></option>				
						
						<? foreach($napravlenie_cat_all as $napravlenie_category): ?>
							<option type="category" <?=(isset($napravlenie_category['current']))? 'selected':''?> value="<?=$napravlenie_category['id']?>"><?=$napravlenie_category['name']?></option>
						<? endforeach; ?> 
												
					</select>	
					
				<? endforeach; ?>

				<input type="text" id="napravlenie_autocomplete" placeholder="название или ИНН" value="<?=$napravlenie_item['name']?>">
			</td>
			
			
			<input type="hidden" name="napravlenie_id" id="napravlenie_id" value="<?=$napravlenie_item['id']?>">
			
			<td>
				<div id="napravlenie_error" class="error"></div>
			</td>			
			
		</tr>
		
		<tr>
			<td>Статус:</td>
			<td>Обследование</td>
		</tr>
		
		<tr id="vid_rabot_tr">
			<td>Вид работ: </td>
			<td id="vid_rabot_td">
				<? 
					$counter = -1; 
					$work_type_names = ['work_type', 'place_type', 'subplace_type_id'];
				?>
				<? foreach($work_type_data as $work_types): ?>
					
					<? 
						$counter++; 					
					?>
					
				
					
					<select attr="<?=$work_type_names[$counter]?>" name="<?=$work_type_names[$counter]?>">
						<option value=""></option> 
						
						<? foreach($work_types as $work_type): ?>
							<? 
								if(!empty($work_type['current']))
								 {
									 $selected = 'selected';
								 }
								else $selected = '';
							?>
							
							<? // print_r($work_type); exit(); ?>
							
							<option attr="<?=$work_type['attribute']?>" <?=$selected?> value="<?=$work_type['id']?>"><?=$work_type['name']?></option>
						
						<? endforeach; ?> 
						
					</select>
				
				<? endforeach; ?>
				
			</td>
			
			<td></td>
		</tr>
			
		<tr>
			<td></td>
			<td class="questions_td">
				
					
				<? if($list_number==1): ?>
				
					<? $question_category = ''; ?>					
						
						<? foreach($questions as $question): ?>
													
							<? if($question_category != $question['category_3']): ?>						   
							   
							   <? if(!empty($question_category)): ?>
									</table>
							   <? endif; ?>
						
							   <? $question_category = $question['category_3']; ?>
							   <h2><?=$question_category?></h2> <a class="questions_delete">X</a>
							   <table class="questions_table">
							   
							<? endif; ?>
								
								<tr>
									<td><?=$question['question']?></td>
									<td class="radio_td">
										<?=$question['answer_1']?>
										<input <?=($question['answer_number']==1)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_1']?>">
										<?=$question['answer_2']?>
										<input <?=($question['answer_number']==2)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_2']?>">
                                        <? if(!empty($question['answer_3'])): ?>
                                        <?=$question['answer_3']?>
                                        <input <?=($question['answer_number']==3)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_3']?>">
                                        <? endif; ?>
                                        <? if(!empty($question['answer_4'])): ?>
                                        <?=$question['answer_4']?>
                                        <input <?=($question['answer_number']==4)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_4']?>">
                                        <? endif; ?>
                                        <? if(!empty($question['answer_5'])): ?>
                                        <?=$question['answer_5']?>
                                        <input <?=($question['answer_number']==5)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_5']?>">
                                        <? endif; ?>
                                        <? if(!empty($question['answer_6'])): ?>
                                            <?=$question['answer_6']?>
                                            <input <?=($question['answer_number']==6)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_6']?>">
                                        <? endif; ?>
									</td>
								</tr>
													
						<? endforeach; ?>
						
						<? if(!empty($questions)): ?>
							</table>
						<? endif; ?>
				
				<? elseif($list_number==2): ?>
					
					<? // если вопросы не заданы ?>
					<? if(empty($questions)): ?>
						
						<? // перебор Чердак, подвал, и т.д. ?>
						<? foreach($place_parts_2 as $place_part_cat_id=>$place_part): ?>
													
							<div class="questions_div">
								<h2><?=$place_part?></h2>
															
								<select class="question_cat_select">
									<option value=""></option>
									<? foreach($question_cats as $question_cat): ?>
										<option value="<?=$question_cat['id']?>"><?=$question_cat['name']?></option>
									<? endforeach; ?>
								</select>
								
								<? foreach($question_cats as $question_cat): ?>
								
									<table class="questions_table checklist_2" question_category_id="<?=$question_cat['id']?>">
										
										<? foreach($questions_raw[$question_cat['id']] as $question): ?>
										
											<tr>
												<td><?=$question['question']?></td>
												<td class="radio_td">
													<? // print_r($question); ?>
                                                    <?=$question['answer_1']?>
                                                    <input <?=($question['answer_number']==1)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_1']?>">
                                                    <?=$question['answer_2']?>
                                                    <input <?=($question['answer_number']==2)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_2']?>">
                                                    <? if(!empty($question['answer_3'])): ?>
                                                        <?=$question['answer_3']?>
                                                        <input <?=($question['answer_number']==3)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_3']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_4'])): ?>
                                                        <?=$question['answer_4']?>
                                                        <input <?=($question['answer_number']==4)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_4']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_5'])): ?>
                                                        <?=$question['answer_5']?>
                                                        <input <?=($question['answer_number']==5)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_5']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_6'])): ?>
                                                        <?=$question['answer_6']?>
                                                        <input <?=($question['answer_number']==6)? 'checked':''?> type="radio" name="question[<?=$question['question_id']?>]" value="<?=$question['answer_6']?>">
                                                    <? endif; ?>

												</td>
											</tr>
											
										<? endforeach; ?>								
										
									</table>
								
								<? endforeach; ?>
								
							</div>
							 
						<? endforeach; ?>
					
					<? // если есть вопросы ?>
					<? else: ?>
					
					
						<? $place_part = ''; $counter = 0; ?>
						
						<? // перебираем массив вопросов ?>
						<? foreach($questions as $question): ?>
							
							<? $counter++; ?>
							
							<? if($place_part != $question['place_part']):	?>
															
								<? $place_part = $question['place_part']; ?>
								
								<? if($counter > 1): ?>									
									</table>
									</div>
								<? endif; ?>
								
								<div class="questions_div">
								
									<h2><? // print_r($question); ?> <?=$question['place_part']?></h2>
									
									<select class="question_cat_select">
										<option value=""></option>
										<? foreach($question_cats as $question_cat): ?>
											<option value="<?=$question_cat['id']?>"><?=$question_cat['name']?></option>
										<? endforeach; ?>
									</select>								
									
									<? // строим таблицу вопросов по умолчанию ?>
									<? foreach($question_cats as $question_cat): ?>
									
										<table class="questions_table checklist_2" question_category_id="<?=$question_cat['id']?>">
											
											<? foreach($questions_raw[$question_cat['id']] as $question_raw): ?>
											
												<tr>
													<td><? // print_r($question_raw); ?><?=$question_raw['question']?></td>
													<td class="radio_td">
														<? // print_r($question); ?>
														<?=$question_raw['answer_1']?>
														<input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_1']?>">
														<?=$question_raw['answer_2']?>
														<input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_2']?>">

                                                        <? if(!empty($question_raw['answer_3'])): ?>
                                                            <?=$question_raw['answer_3']?>
                                                            <input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_3']?>">
                                                        <? endif; ?>
                                                        <? if(!empty($question_raw['answer_4'])): ?>
                                                            <?=$question_raw['answer_4']?>
                                                            <input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_4']?>">
                                                        <? endif; ?>
                                                        <? if(!empty($question_raw['answer_5'])): ?>
                                                            <?=$question_raw['answer_5']?>
                                                            <input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_5']?>">
                                                        <? endif; ?>
                                                        <? if(!empty($question_raw['answer_6'])): ?>
                                                            <?=$question_raw['answer_6']?>
                                                            <input disabled class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question_cat['id']?>][<?=$question_raw['id']?>]" value="<?=$question_raw['answer_6']?>">
                                                        <? endif; ?>
													</td>
												</tr>
												
											<? endforeach; ?>								
											
										</table>
									
									<? endforeach; ?>
			
									<? // строим таблицу вопросов загруженных из базы ?>
									<table class="questions_table">
								
							<? endif; ?>
							
										<tr>
											<td>
												<?=$question['question']?>
												<? // print_r($question); ?>									
											</td>
											<td class="radio_td">
												<?=$question['answer_1']?>
												<input <?=($question['answer_number']==1)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_1']?>">
												<?=$question['answer_2']?>
												<input <?=($question['answer_number']==2)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_2']?>">

                                                <? if(!empty($question['answer_3'])): ?>
                                                    <?=$question['answer_3']?>
                                                    <input <?=($question['answer_number']==3)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_3']?>">
                                                <? endif; ?>
                                                <? if(!empty($question['answer_4'])): ?>
                                                    <?=$question['answer_4']?>
                                                    <input <?=($question['answer_number']==4)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_4']?>">
                                                <? endif; ?>
                                                <? if(!empty($question['answer_5'])): ?>
                                                    <?=$question['answer_5']?>
                                                    <input <?=($question['answer_number']==5)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_5']?>">
                                                <? endif; ?>
                                                <? if(!empty($question['answer_6'])): ?>
                                                    <?=$question['answer_6']?>
                                                    <input <?=($question['answer_number']==6)? 'checked' : ''?> class="question_radio" type="radio" name="question[<?=$question['place_part_id']?>][<?=$question['category_3_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_6']?>">
                                                <? endif; ?>



											</td>
										</tr>
							
						<? endforeach; ?>
						
						</table>
						</div>
						
					<? endif; ?>
				

				<? elseif($list_number==3): ?>
					
					<!-- <h2>Результаты</h2> -->
					
						
					<? if(empty($questions)): ?>

						<? foreach($place_parts_3 as $place_part_cat_id=>$place_part): ?>
														
								<div class="questions_div">
									<h2><?=$place_part?></h2>
																													
									<table class="questions_table checklist_3">
										
										<? foreach($questions_raw as $question): ?>
										
											<tr>
												<td><?=$question['question']?></td>
												<td class="radio_td">
													<? // print_r($question); ?>
													<?=$question['answer_1']?>
													<input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_1']?>">
													<?=$question['answer_2']?>
													<input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_2']?>">

                                                    <? if(!empty($question['answer_3'])): ?>
                                                        <?=$question['answer_3']?>
                                                        <input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_3']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_4'])): ?>
                                                        <?=$question['answer_4']?>
                                                        <input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_4']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_5'])): ?>
                                                        <?=$question['answer_5']?>
                                                        <input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_5']?>">
                                                    <? endif; ?>
                                                    <? if(!empty($question['answer_6'])): ?>
                                                        <?=$question['answer_6']?>
                                                        <input class="question_radio" type="radio" name="question[<?=$place_part_cat_id?>][<?=$question['id']?>]" value="<?=$question['answer_6']?>">
                                                    <? endif; ?>
												</td>
											</tr>
											
										<? endforeach; ?>								
										
									</table>					
								</div>
								 
							<? endforeach; ?>					
					
						
					<? else: ?>
						 
					    <? // если вопросы есть ?>	
						<? $place_part = ''; $counter = 0; ?>
						<? foreach($questions as $question): ?>
							
							<? if($place_part != $question['place_part']): ?>
								<? $place_part = $question['place_part']; $counter++; ?>
								<? if($counter > 1): ?>
									</table>
									</div>
								<? endif; ?>
									
									<div class="questions_div">
									<h2><?=$place_part?></h2>
																													
									<table class="questions_table checklist_3">
									
							<? endif; ?>
						
							<tr>
								<td><? // print_r($question); ?> <?=$question['question']?></td>
								<td class="radio_td">
									<?=$question['answer_1']?> 
									<input type="radio" <?=($question['answer_number']==1)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_1']?>">
									<?=$question['answer_2']?> 
									<input type="radio" <?=($question['answer_number']==2)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_2']?>">
                                    <? if(!empty($question['answer_3'])): ?>
                                        <?=$question['answer_3']?>
                                        <input type="radio" <?=($question['answer_number']==3)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_3']?>">
                                    <? endif; ?>
                                    <? if(!empty($question['answer_4'])): ?>
                                        <?=$question['answer_4']?>
                                        <input type="radio" <?=($question['answer_number']==4)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_4']?>">
                                    <? endif; ?>
                                    <? if(!empty($question['answer_5'])): ?>
                                        <?=$question['answer_5']?>
                                        <input type="radio" <?=($question['answer_number']==5)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_5']?>">
                                    <? endif; ?>
                                    <? if(!empty($question['answer_6'])): ?>
                                        <?=$question['answer_6']?>
                                        <input type="radio" <?=($question['answer_number']==6)? 'checked' : ''?> name="question[<?=$question['place_part_id']?>][<?=$question['question_id']?>]" value="<?=$question['answer_6']?>">
                                    <? endif; ?>
								</td>
							</tr>
							
						<? endforeach; ?>	
						
						</table>
						</div>
						
					<? endif; ?>
										
				
				<? endif; ?>
				
				
				
				<? if($list_number==1): ?>	
					
					<? if(!empty($place_parts)): ?>
						<a class="place_parts_open" href="#">+</a>
					<? endif; ?>
					
				<? endif; ?>
					
				</td>
				
				<td></td>
				
				<td>
					<div id="work_type_error" class="error"></div>
				</td>				
		</tr>
		
		<tr>
			<td>Дата: </td>
			<td>
				<input id="date_select" type="text" name="date" value="<?=$title_list['date']?>">
			</td>
			
			<td>
				<div id="date_error" class="error"></div>
			</td>				
		</tr>
			
		<tr id="workers_tr" style="<?=($operation!='view')? 'display: none;' : ''?>">
			<td>Работники: </td> 
			<td>
				<select id="workers_list" name="workers"> <!-- multiple -->
					<? foreach($workers as $worker): ?>
						<option <?=(in_array($worker['id'], $workers_selected))? 'selected' : ''?> value="<?=$worker['id']?>"><?=$worker['name']?> <?=$worker['surname']?></option>
					<? endforeach; ?>
				</select>
			</td>
			
			<td>
				<div id="workers_error" class="error"></div>
			</td>				
		</tr>
		
		<!-- <input type="hidden" name="MAX_FILE_SIZE" value="30000" /> -->
		 
		<tr>
			<td>Загрузка скана</td>
			<td>
				<span class="btn btn-success fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Выберите файл...</span>
					<!-- The file input field used as target for the file upload widget -->
					<input id="scan_upload" type="file" name="scan"> <!-- multiple -->
				</span>
												
				<div id="files">					
					<? if(!empty($scanlist)): ?>
						<a target="blank" href="/data/scans/<?=$scanlist?>"><?=$scanlist?></a>
					<? endif; ?>								
				</div>
								
				<input type="hidden" name="scanlist" id="scanlist_name">
				 
				<div id="progress" class="progress">
					<div class="progress-bar progress-bar-success"></div>
				</div>		
			</td>
			<td>
				<div id="scan_upload_error" class="error"></div>
			</td>
		</tr> 
		
		
		<tr>
			<td align="center" colspan="2">
				<div class="message"></div>
				<input type="submit" name="submit" id="list_save_button" value="Сохранить">				
				<button id="print_button" onclick="window.print(); return false;">Печать</button>
				<? if(!$signed): ?> <button id="list_sign">Подписать</button> <? endif; ?>
			</td>
		</tr>
		
	</table>
	
</form>

<select id="place_parts_dist">
	<option value=""></option>
	<? foreach($place_parts as $place_part): ?>
		<option value="<?=$place_part['id']?>"><?=$place_part['name']?></option>
	<? endforeach; ?>
</select>


</body>
</html>
