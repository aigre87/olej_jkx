$(document).ready(function()
{			
	// загрузка файла
    $('input#scan_upload').fileupload({
        url: '?action=scan_upload',
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(pdf|jpe?g|png)$/i,
		done: function (e, data) 
		 {
            // alert('ok');
			$('div#scan_upload_error').html('');
			$('.fileinput-button').remove();			
			
			$.each(data.result, function (index, file) 
			 {
                $('div#files').html('<a href="/data/scans/'+file.name+'">'+file.name+'</a>');
				// alert('test');
				$('input#scanlist_name').val(file.name);
				// alert(file.name); 
             });
         },
        progressall: function (e, data) 
		 {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width', progress + '%');
        }
	   }).on('fileuploadprocessalways', function(e, data)
	    {			
			var index = data.index,
				file = data.files[index];
				// node = $(data.context.children()[index]);
		        if(file.error)
				 {
					 $('div#scan_upload_error').html( file.error );
				 }
		}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	
	
	// проверка отключенности
	if($('input#disabled_edit').val() == 1)
	 {
		 $('form#checklist_edit_form :input').attr('disabled', true);
		 $('input#list_save_button').remove();
		 $('button#print_button').removeAttr('disabled');
		 $('button#list_sign').removeAttr('disabled');
		 $('a.place_parts_open').remove();
	 }
	
	if($('input#disabled_work_change').val() == 1)
	 {
		 $('td#vid_rabot_td select').attr('disabled', true);
		 /*
		 $('table.questions_table').find('input').attr('disabled', true);
		 $('a.place_parts_open').remove();
		 */
	 }
	
	$('#date_select').datepicker({onSelect: function()
								   {
									  $('div#date_error').html('');
									  var date_text = $('#date_select').val();
									  date_text     = date_text.replace(/\./g,'');  
									  $('div#date_number').text(date_text);
								   }
								 });
	
	function error_check()
	 {
		$('div.error').html('');
		
		// проверка ошибок 
		var error = 0;
		

		if($("input[name^=question]").length == 0)
		 {
			 error = 1;
			 if($('div#work_type_error').html() != '')
			  {
				  $('div#work_type_error').append(', ');
			  }
			 $('div#work_type_error').append('не задан опросник');
		 }
	    else if( $('td.radio_td').not(':has(:radio:disabled)').filter(':has(:radio:checked)').size() == 0 )
		 {
			 error = 1;
			 $('div#work_type_error').append(' опросник пуст');
		 }
		else if( $('td.radio_td').not(':has(:radio:disabled)').not(':has(:radio:checked)').size() > 0)
		  {
			  error = 1;
			  $('div#work_type_error').append(' не все вопросы отвечены');
		  }
		 
		// alert( $('td.radio_td').not(':has(:radio:disabled)').filter(':has(:radio:checked)').size() );

		
		if($('select#brigada_select').val() == '')
		 {
			 error = 1;
			 $('div#brigada_error').html('Не задана бригада');
		 }
		else
		 {
			if($("select#workers_list").val() == null)
			 {
				 $('div#workers_error').html('Не заданы работники');
			 }			 
		 }
		
		if($("input[name='adress[index]']").val() == '' ||
		   $("input[name='adress[index]']").val() == '' ||
		   $("input[name='adress[city]]']").val() == '' ||
		   $("input[name='adress[street]']").val() == '' ||
		   $("input[name='adress[dom]']").val() == '')
		    {
			   error = 1;
			   if($('input#adress').val() == '')
			    {
				  $('div#adress_error').html('Адрес не задан');
				}
			   else
			    {
				  $('div#adress_error').html('Адрес задан в неправильной форме');
				}
		    }
								
		if($('input#napravlenie_id').val() == '')
		 {
			 error = 1;
			 $('div#napravlenie_error').html('Не задано направление');			 
		 }
		
		var place_type = $("select[attr='place_type']").find('option:selected').attr('attr');
		// alert(place_type);
		if(typeof($("select[name='subplace_type_id']").val()) == 'undefined' && place_type != 'last')
		 {
			 error = 1;
			 $('div#work_type_error').html('Не задан тип работ');
		 }
		

		
		if($("input[name='date']").val() == '')
		 {
			 error = 1;
			 $('div#date_error').html('Не задана дата');
		 }
		 
        return error;		 
	 }
	
	$(document).on('change', 'select.question_cat_select', function()
	 {
		 var parent_div = $(this).parents('div.questions_div');
		 var question_cat_id = $(this).val();
		 $(parent_div).find('table.questions_table').hide();
		 $(parent_div).find('table.questions_table').find('input').attr('disabled', true);
		 		 
		 $(parent_div).find('table.questions_table[question_category_id="'+question_cat_id+'"]').find('input').attr('disabled', false);
		 $(parent_div).find('table.questions_table[question_category_id="'+question_cat_id+'"]').show();
		 
		 return false;
	 });
	
	$(document).on('click', 'input#list_save_button', function()
	 {				
		var error = error_check();
		
		if(error) {	 return false;  }

		var trigger = $(this);
				
		$.ajax({url: '/?action=list_edit',
			    method: 'POST', 
			    data: $('form#checklist_edit_form').serialize(),
			    dataType: 'json',
			    success: 
				  function(result)
				   {
						if(result.status == 0)
						 {
							 $.each(result.errors, function(index, error)
							  {
								 alert('Внутренняя ошибка: ' + error); 
							  });
						 }
						else
						 {
							 if($('input#checklist_number').val() == 2)
							  {
								  if($('a.list_3_edit_link').size() == 0 && $('a.list_3_create_link').size() == 0)
								   {
									   var checklist_id = $('input#checklist_id').val();
									   $('div#list_menu').append('<a href="/?action=list_view&list_id='+checklist_id+'&page=3">Создать лист результатов</a>');
									   // $('div#list_names').append('<h1>Создание листа результатов</h1>');
								   }
							  }
							  
							 $(trigger).parent('td').children('div.message').show().text('Сохранено').delay(1000).fadeOut(); 
						 }
				   }});					   
		
		return false;
	 });
	
	$(document).on('click', 'button#list_sign', function()
	 {
		var error = error_check();
		if(error)
		 {
			 return false;
		 }
		
		var checklist_id 	 = $('input#checklist_id').val();
		var checklist_number = $('input#checklist_number').val();
		var checklist_number_next = parseInt(checklist_number) + 1;
		
		// alert('#list_'+checklist_number_next+'_create_link');
		if(checklist_number_next <= 3)
		 {
			$('#list_'+checklist_number_next+'_edit_link').show();
			$('#list_'+checklist_number_next+'_create_link').show();
		 }
		// return false;
		
		
		$.ajax({url: '/?action=list_sign',
			    method: 'POST', 
			    data: {checklist_id: checklist_id, checklist_number: checklist_number},
			    dataType: 'json',
			    success: 
				  function(result)
				   {
					  $('button#list_sign').remove();
				   }});
				   
		return false;
	 });
	
	$(document).on('click', '#workers_list', function()
	 {
		$('div#workers_error').html(''); 
	 });
	
	$(document).on('click', '#list_create_button', function()
	 {	    
		var error = error_check();
		
		if(error)
		 {
			 return false;
		 }			
		
		// alert('creation'); return false;
		
		$.ajax({url: '/?action=list_save_new',
			    method: 'POST', 
			    data: $('#checklist_create_form').serialize(),
			    dataType: 'json',
			    success: 
				  function(result)
				   {
					  if(result.status == 0)
					   {
						   alert('произошла внутренняя ошибка при создании листа');
						   return false;
					   }
					  
					  window.location.href = '/?action=list_view&list_id=' + result.checklist_id;
					  
				   }});
	    return false;
	 });
	
	$(document).on('change', 'select#brigada_select', function()
	 {
		$('div#brigada_error').html('');
		$('select#workers_list').html('');
		
		if($(this).val() == '')
		 {
			 $('tr#workers_tr').hide();
			 return false;
		 }
		 
		var brigada_id = $(this).val();
		
		$('div#brigada_number').text(brigada_id);
		
	    $.ajax({url: '/?action=workers_get',
			    method: 'POST', 
			    data: {brigada_id: brigada_id},
			    dataType: 'json',
			    success: 
				  function(result)
				   {
					  $('tr#workers_tr').show();
					  
					  $.each(result, function(index, elem)
					   {
						  $('select#workers_list').append('<option value="'+elem.id+'">'+elem.name+' '+elem.surname+'</option>');
					   });
				   }});
	 });
	
	$(document).on('click', 'a.questions_delete', function()
	 {
		$(this).prev('h2').remove();
		var question_table = $(this).next('table.questions_table');
		var category_id    = $(question_table).children('input.question_cat_id').val();
		$('select#place_parts_dist option[value='+category_id+']').show();
		$(question_table).remove();
		$(this).remove();
	 });
	
	$(document).on('change', 'select.place_parts', function()
	 {
		$('div#work_type_error').html('');
		
		var category_id = $(this).val();
		var checklist_number  = $('input#checklist_number').val();
		var selector    = $(this);
		var parts_open  = $(this).parent().children('a.place_parts_open');
		
		if(category_id == '')
		 {
			 return false;
		 }
		
		// $('select#place_parts_dist option[value='+category_id+']').remove();
		$('select#place_parts_dist option[value='+category_id+']').hide();
		
	    $.ajax({url: '/?action=questions_get',
			    method: 'POST', 
			    data: {category_id: category_id, checklist_number: checklist_number},
			    dataType: 'json',
			    success: 
				  function(result)
				   {
					  var questions_td = $(selector).parent('td.questions_td');
					  
					  // alert ( $(selector).children('option:selected').text() );
					  $(selector).replaceWith( '<h2>' + $(selector).children('option:selected').text() + '</h2><a class="questions_delete">X</a>' );
					  $(parts_open).hide();					  					  
					  
					  // alert('ok');
					  
					  /*
					  // надо ли?
					  // $('table#checklist_create_table #vid_rabot_tr').after('<tr><td></td><td class="questions_td"></td></tr>');
					  */
					  
					  
					  $(questions_td).append('<table class="questions_table">');
					  var questions_table = $('table.questions_table').last();
					  $(questions_table).append('<input type="hidden" class="question_cat_id" value="'+category_id+'">');
					  
					  $.each(result, function(index,elem)
					   {
						//  $(questions_table).append('<tr><td>'+elem.question+'</td><td class="radio_td"><input type="radio" name="question['+elem.id+']" value="1">'+elem.answer_1+'<input type="radio" name="question['+elem.id+']" value="2">'+elem.answer_2+'</td></tr>');

                           answer_3 = '';
                           answer_4 = '';
                           answer_5 = '';
                           answer_6 = '';
                           if(elem.answer_3) {
                               answer_3 = '<input type="radio" name="question['+elem.id+']" value="'+elem.answer_3+'">' + elem.answer_3 + ' ';
                           }
                           if(elem.answer_4) {
                               answer_4 = '<input type="radio" name="question['+elem.id+']" value="'+elem.answer_4+'">' + elem.answer_4 + ' ';
                           }
                           if(elem.answer_5) {
                               answer_5 = '<input type="radio" name="question['+elem.id+']" value="'+elem.answer_5+'">' + elem.answer_5 + ' ';
                           }
                           if(elem.answer_6) {
                               answer_6 = '<input type="radio" name="question['+elem.id+']" value="'+elem.answer_6+'">' + elem.answer_6 + ' ';
                           }


                           $(questions_table).append('<tr><td>'+elem.question+'</td><td class="radio_td"><input type="radio" name="question['+elem.id+']" value="'+elem.answer_1+'">'+elem.answer_1+'<input type="radio" name="question['+elem.id+']" value="'+elem.answer_2+'">'+elem.answer_2 + answer_3 + answer_4 + answer_5 + answer_6 +'</td></tr>');
					   });  
					  
					  
					  if($('select#place_parts_dist option').size() == 1)
					   {
						  return false;
					   }
					  
					  var questions_td_new = $('td.questions_td').last();
					  $(questions_td_new).append('<a class="place_parts_open" href="#">+</a><br>');					  
					  
				   }});
	 });
	
	$(document).on('click', 'a.place_parts_open', function()
	 {
		 // alert( $('select#place_parts_dist').html() );
		 if($('select#place_parts_dist option').size() == 1)
		  {
			  return false;
		  }
		 
		 $(this).hide();
		 $(this).parent().append('<select class="place_parts"></select>');
		 $(this).parent().children('select.place_parts').append( $('select#place_parts_dist').html() );
		 $(this).parent().children('select.place_parts').show('slow');
		 
		 return false;
	 });
	
	$(document).on('change', '#vid_rabot_td select', function()
	 {	 		 
		 $('div#work_type_error').html('');
		 		 
		 var work_type_id = $(this).val();
		 var selector = $(this);
		 
		 if($(this).attr('attr') == 'work_type')
		  {
			  var work_type_name = $(this).children('option:selected').text();			  
			  switch(work_type_name)
			   {
				   case 'Дезинсекция': work_type_name = 'ДC'; break;
				   case 'Дезинфекция': work_type_name = 'ДФ'; break;
				   case 'Дератизация': work_type_name = 'ДР'; break;
				   case 'Акарицидная обработка': work_type_name = 'АК'; break;			   
				   case 'Противомалярийная обработка': work_type_name = 'МЛ'; break;
			   }
			  $('div#title_work_type').text(work_type_name);
		  }
		 
		  $(selector).nextAll().remove();
		  $('.questions_td').remove();
		  $('select#place_parts_dist').html('');
		 
		 if(work_type_id == '')
		  {
			  $(selector).nextAll().remove();
			  return false;
		  }		  
		 
		 // alert($(this).val());
		 // var work_type  = $("select[attr='work_type']").find('option:selected').text();
		 // var place_type = $("select[attr='place_type']").find('option:selected').text();
		 // alert(work_type); alert(place_type);
		 // alert( $(this).attr('attr_1') );
		 // alert( $(this).find('option:selected').attr('attr') );
		 
		 if($(this).attr('attr') == 'subplace_type_id' || $(this).find('option:selected').attr('attr') == 'last')
		  {			  			   
			   // вызываем выбор типа здания из другой таблицы
			   // получаем триггеры
				var work_type  = $("select[attr='work_type']").find('option:selected').text();
				var place_type = $("select[attr='place_type']").find('option:selected').text();
			    var check_list = $("input#checklist_number").val();
			   
			   // загружаем данные из таблицы
			   $.ajax({url: '/?action=place_parts_get',
					   method: 'POST', 
					   data: {work_type: work_type, place_type: place_type, check_list: check_list},
					   dataType: 'json',
					   success: 
						 function(result)
						  {
							  // alert('1');
							  
							  if(result.length == 0)
							   {
								   return false;
							   }
							   
							  if($('table#checklist_edit_table').size() == 1)
							   {
								  $('#checklist_edit_table tr#vid_rabot_tr').after('<tr><td><td class="questions_td">');
								  var questions_table = $('#checklist_edit_table').find('td.questions_td').last();	  
							   }
							  else
							   {
								  $('#checklist_create_table tr#vid_rabot_tr').after('<tr><td><td class="questions_td">');
								  var questions_table = $('#checklist_create_table').find('td.questions_td').last();
							    }

							  
							  // alert('test');
							  $(questions_table).append('<a class="place_parts_open" href="#">+</a><br>');
							  
							  $('select#place_parts_dist').append('<option value=""></option>');
							  
							  $.each(result, function(index, elem)
							   {
								   $('select#place_parts_dist').append('<option value="'+elem.id+'">'+elem.name+'</option>');
							   });
							  
						  }});

			   return false;	
		  }		  
		  
		 
		 $.ajax({url: '/?action=work_types_get', 
				 method: 'POST', 
				 data: {work_type_id: work_type_id},
				 dataType: 'json',
				 success: 
			 function(result)
			  {
				  
				  if(result.length == 0)
				   {
					   return false;
				   }
				  
				  $('#vid_rabot_td').append('<select>');
				  
				  var current_select = $('#vid_rabot_td').children('select').last();
				  
				  if($('td#vid_rabot_td select').size() == 2)
				   {
					  $(current_select).attr('attr', 'place_type');
					  $(current_select).attr('name', 'place_type');
				   }
				  
				  if($('td#vid_rabot_td select').size() == 3)
				   {
					  $(current_select).attr('attr', 'subplace_type_id');
					  $(current_select).attr('name', 'subplace_type_id');
				   }
				  
				  $(current_select).append('<option value=""></option>');
				  

				  $.each(result, function(i, elem)
				   {				   
					   // $(current_select).attr('attr_1', elem.attribute);
					   
					   /*
					   if(elem.attribute != '')
					    {
							$(current_select).attr('attr', elem.attribute);
							if(elem.attribute == 'last')
							 {
								$(current_select).attr('name', 'subplace_type_id');
							 }
							else
							 {
								$(current_select).attr('name', elem.attribute);
							 }
							
							if(elem.attribute == 'last')
							 {
								 // $(current_select).attr('multiple', true);
							 }
						}
					   */	
					   $(current_select).append('<option value="'+elem.id+'">'+elem.name+'</option>'); // .attr('attr', attribute_name);
					   $(current_select).children('option').last().attr('attr', elem.attribute);
				   }); 
				  
			  }});
	 });
	
	$(document).on('change', '#napravlenie_select_td select', function()
	 {
		 // alert('123');
		 
		 $('div#napravlenie_error').html('');
		 // получаем тип
		 // alert( $(this).attr('type') );
		 var type     = $(this).children('option:selected').attr('type');
		 var id       = $(this).children('option:selected').val();
		 var selector = $(this);
		 
		 // alert(id);
		 $(this).nextAll().remove();
		 if(id == '')
		  {
			  // $(this).nextAll().remove();
			  return false;
		  }
		 
		 // открываем окно ввода
		 if($('#napravlenie_select_td select').size() == 2)
		  {
			  var napravlenie_search_url = '?action=napravlenie_search';
			  $('#napravlenie_select_td').append('<input type="text" id="napravlenie_autocomplete" placeholder="название или ИНН">');			  
			  $('#napravlenie_autocomplete').autocomplete({minLength: 1, source: function(request, response)
			   {
					 var search_inn = 0;
					 if($.isNumeric($('input#napravlenie_autocomplete').val()) && $('input#napravlenie_autocomplete').val().length == 10)
					  {
						  search_inn = 1;
					  }
					  
					 $.ajax({url: '/?action=napravlenie_search', 
								 method: 'POST', 
								 // data: {request: request.term, category_id: id, search_inn: search_inn},
								 data: {request: request.term, category_id: id},
								 dataType: 'json',
								 success: function(result)
								  {
										response(result);
								  }});
			   },
			  select: function(event, ui)
			   {
					 $('input#napravlenie_id').val(ui.item.value);
					 $('input#napravlenie_autocomplete').val(ui.item.label);
					 return false;
			   },
			  search: function(event, ui)
			   {
				  if($.isNumeric($('input#napravlenie_autocomplete').val()) && $('input#napravlenie_autocomplete').val().length < 10)
				   {
					   // return false;
				   }
			   }});
			  	
				
			  return false;
		  }
		 
	 
		 $.ajax({url: '/?action=napravlenie_get', 
				 method: 'POST', 
				 data: {category_id: id},
				 dataType: 'json',
				 success: 
			 function(result)
			  {
				 $(selector).nextAll().remove();
				 
				 if(result.length == 0)
				  {
					  return false;
				  }
				 
				 // $(selector).hide();					 
									 
				 $('#napravlenie_select_td').append('<select>');
				 
				 var current_select = $('#napravlenie_select_td').children('select').last();
				 
				 $(current_select).append('<option value=""></option>');
				 
				 $.each(result, function(i, elem)
				  {
					$(current_select).append('<option type="'+elem.type+'" value="'+elem.id+'">'+elem.name+'</option>');
				  });				  			  
							
			  }}); 	

	 });


	$("#adress").suggestions({
        token: "4cdaa1e9bfe7d3c31e8265030423bb16db08af4e",
        type: "ADDRESS",
        count: 5,
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion) 
		 {
			$('div#adress_error').html('');
			// alert(suggestion.data.street);
			// alert(suggestion.data.postal_code);
			$("input[name='adress[index]']").val(suggestion.data.postal_code);
			$("input[name='adress[city]']").val(suggestion.data.city);
			$("input[name='adress[street]']").val(suggestion.data.street);
			$("input[name='adress[rajon]']").val(suggestion.data.city_district);
			$("input[name='adress[okrug]']").val(suggestion.data.city_area);
			$("input[name='adress[dom]']").val(suggestion.data.house);
			$("input[name='adress[block]']").val(suggestion.data.block);
			$("input[name='adress[geocoord_lat]']").val(suggestion.data.geo_lat);
			$("input[name='adress[geocoord_lon]']").val(suggestion.data.geo_lon);
         }
    });
});