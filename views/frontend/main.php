<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Главная</title>
	<link rel="stylesheet" href="/front_build/css/template_styles.min.css">
	<link rel="stylesheet" href="/front_build/css/fonts.min.css">
	<script src="/front_build/libs/libs.min.js"></script>
	<script src="/front_build/js/main.min.js"></script>
</head>
<body>
	<div id="mainWrapper">
		<div class="homepage">
			<header>
				<div class="layout">
					<div class="title">
						Добро пожаловать в закрученную хитрую систему ЖКХ
					</div>
					<div class="user">
						<div class="userInfo">
							<div class="name">
								<?=$this->user_data['name']?> 
								<?=$this->user_data['middlename']?> 
								<?=$this->user_data['surname']?>
							</div>
							<div class="organization">
								<?=$this->organization_data['name']?>
							</div>
						</div>
						<div class="logout">
							<a href="?action=logout">
								<span class="txt">выход</span>
								<svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/front_build/images/symbol/sprite.svg#ico-logout"></use></svg>
							</a>
						</div>
					</div>
				</div>
			</header>
			<div class="headerMenu">
				<div class="layout">
					<div class="item">
						<a href="/">список листов</a>
					</div>
					<div class="item">
						<a href="?action=list_create">создать обходной лист</a>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="layout">
				<? // print_r($checklists); ?>
				<? if(count($checklists) > 0): ?>

					<? 
						$color_1 = 'green';
						$color_2 = 'yellow';
						$color_3 = 'blue';
					?>

					<table>
						<tr>
							<th>Организация</th>
							<th>Направление</th>	
							
							<th>№1</th>
							<th>Бригада</th>
							<th>Работник</th>
							<th>Статус</th>

							<th>№2</th>
							<th>Бригада</th>
							<th>Работник</th>
							<th>Статус</th>		

							<th>№3</th>
							<th>Бригада</th>
							<th>Работник</th>
							<th>Статус</th>		
						</tr>

						<? foreach($checklists as $checklist): ?>
							<tr>
								<td><?=$checklist['organization_name']?></td>
								<td><?=$checklist['napravlenie_name']?></td>
								
								<? $color_bg = $color_1; ?>
								
								<td class="<?=$color_bg?>"><a href="/?action=list_view&list_id=<?=$checklist['id']?>"><?=(!empty($checklist['number_1']))? $checklist['number_1'] : '-'?></a></td>
								<td class="<?=$color_bg?>"><?=($checklist['brigada_1'])? $checklist['brigada_1'] : ''?></td>
								<td class="<?=$color_bg?>"><?=$checklist['worker_1_name']?></td>
								<td class="<?=$color_bg?>">Обследование</td>
								
								
								<? $color_bg = ($checklist['date_2']!='0000-00-00')? $color_2 : '' ?>
								
								<td class="<?=$color_bg?>"><a href="/?action=list_view&list_id=<?=$checklist['id']?>&page=2"><?=(!empty($checklist['number_2']))? $checklist['number_2'] : '-'?></a></td>
								<td class="<?=$color_bg?>"><?=($checklist['brigada_2'])? $checklist['brigada_2'] : ''?></td>
								<td class="<?=$color_bg?>"><?=$checklist['worker_2_name']?></td>
								<td class="<?=$color_bg?>">Истребление</td>

								
								<? $color_bg = ($checklist['date_3']!='0000-00-00')? $color_3 : '' ?>
								
								<td class="<?=$color_bg?>"><a href="/?action=list_view&list_id=<?=$checklist['id']?>&page=3"><?=(!empty($checklist['number_3']))? $checklist['number_3'] : '-'?></a></td>
								<td class="<?=$color_bg?>"><?=($checklist['brigada_3'])? $checklist['brigada_3'] : ''?></td>
								<td class="<?=$color_bg?>"><?=$checklist['worker_3_name']?></td>
								<td class="<?=$color_bg?>">Результат</td>
							</tr>
						<? endforeach; ?>
						
					</table>

					<a class="defaultButton" href="/?show_all=1">Показать всё</a>

				<? else: ?>
					Листы не найдены в системе
				<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>