<?
class model
 {
	 private $db;
	 private $shorters;
	 
	 function __construct()
	  {
		  require_once 'config.php';
		  $this->shorters = $shorters;
		  require_once 'libs/safemysql.class.php';
		  $this->db = SafeMySQL::start($config['mysql']);
	  }
	
	 function clear()
	  {
		  $this->db->query("TRUNCATE `checklists`");
		  $this->db->query("TRUNCATE `questions_data`");
	  }
	 
	 function list_view_data_build($list_id, $list_number, $organization_data)
	  {
		   $data['list_number']      = $list_number;
		   $data['title_list']  	 = $this->list_get($list_id);
		   
		   if(!$data['title_list'])
		    {
				return ['status' => 0, 'error' => 'Лист не найден в системе'];
			}

		   $data['title_list']['date'] = $data['title_list']['date_'.$list_number];
		   $data['questions']   	 = $this->answers_get($list_id, $list_number);
		   $data['organization'] 	 = $organization_data;	
		   $data['brigadu']     	 = $this->brigadu_get($data['organization']['id']);
		   $data['workers']          = $this->workers_get($data['title_list']['brigada_'.$list_number]);
		   $data['napravlenie_data'] = $this->napravlenie_build($data['title_list']['napravlenie_id']);
		   $data['napravlenie_item'] = $this->napravlenie_get($data['title_list']['napravlenie_id']);
		   $data['napravlenie_item']['name'] = htmlspecialchars($data['napravlenie_item']['name']);
		   $place_type_id = (!empty($data['title_list']['subplace_type_id']))? $data['title_list']['subplace_type_id'] : $data['title_list']['place_type_id'];
		   $data['work_type_data']   = $this->work_type_build($place_type_id);
		   $data['operation']        = 'view';
		   $data['workers_selected'] = explode(',', $data['title_list']['workers_'.$list_number]);		   
		   $data['scanlist']         = $data['title_list']['scanlist_'.$list_number];
		   $data['signed']           = !empty($data['title_list']['number_'.$list_number]);
		   $data['title_list']['number'] = $data['title_list']['number_'.$list_number];
		   $data['organization_data'] = $this->organization_get($data['title_list']['organization_id']);
		   $data['lists_per_day']     = $this->lists_created_per_day();
		   $data['title_list']['brigada'] = $data['title_list']['brigada_'.$list_number];
		   // echo $data['title_list']['date']; exit();
		   
		   foreach($data['work_type_data'][0] as $item)
		    {
				if(isset($item['current']))
				 {
					 $data['work_type'] = $item['name'];					 
					 break;
				 }
			}

		   // print_r($data['work_type_data']);
		   
		   foreach($data['work_type_data'][1] as $item)
		    {
				if(isset($item['current']))
				 {
					 $place_type = $item['name'];
					 break;
				 }
			}			
		   // получаем части здания
		   $data['place_parts']      = $this->place_parts_get($data['work_type'], $place_type, $list_number);
		   
		   // готовим опросник для первого листа
		   if($list_number == 1)
		    {
			   if(!empty($data['place_parts']))
				{
				   foreach($data['place_parts'] as $key=>$place_part)
					{
						// print_r($place_part);
						foreach($data['questions'] as $question)
						 {
							 if($question['category_3_id'] == $place_part['id'])
							  {
								 unset($data['place_parts'][$key]);
							  }
						 }
					}
				}
			}
		   			

		   // готовим опросник для второго листа
		   elseif($list_number==2)
		    {				
				// tmp plug
				// $data['questions'] = [];
				
				// выводим стандартные вопросы
				$data['questions_1']   	 = $this->answers_get($list_id, 1);
				foreach($data['questions_1'] as $question)
				 {
					$questions_cat[$question['category_3_id']] = $question['category_3'];
				 }			   
				
				$data['place_parts_2'] = array_unique($questions_cat);
				
				
				// получаем вопросы
				$work_type_id  = $this->question_cat_get_by_name($data['work_type'], $list_number);
				$data['question_cats'] = $this->question_cats_get_by_id($work_type_id, $list_number);
				// exit('123');
				foreach($data['question_cats'] as $question_cat)
				 {
					$data['questions_raw'][$question_cat['id']] = $this->questions_get($question_cat['id'], $list_number);
				 }
			}   
		   // готовим опросник для третьего листа
		   elseif($list_number==3)
		    {
				$data['questions_1']   	 = $this->answers_get($list_id, 1);
				foreach($data['questions_1'] as $question)
				 {
					$questions_cat[$question['category_3_id']] = $question['category_3'];
				 }			   				
				$data['place_parts_3'] = array_unique($questions_cat);
			
				// генерируем список вопросов для вывода
				$work_type_id = $this->question_cat_get_by_name($data['work_type'], $list_number);
				// exit($work_type_id);
				$data['questions_raw'] = $this->questions_get($work_type_id, $list_number);
				// print_r($data['questions_raw']); exit();
			}
			
			
			
		   $data['title_list']['date'] = ($data['title_list']['date'] != '00.00.0000')? $data['title_list']['date'] : '';
		   $data['status'] = 1;
		   
		   return $data;
	  }
	 
	 function checklists_get_by_org($organization_available, $show_all)
	  {
		  $limit = ($show_all)? '' : 'LIMIT 15';
		  
		  $organization_available = explode(',', $organization_available);
		  
		  $sql = "SELECT `checklists`.*,
						 `organizations`.name as organization_name,
						 `napravlenie_category`.name as napravlenie_name,
						 CONCAT(workers_1.name, ' ', workers_1.surname) as worker_1_name,
						 CONCAT(workers_2.name, ' ', workers_2.surname) as worker_2_name,
						 CONCAT(workers_3.name, ' ', workers_3.surname) as worker_3_name
				  FROM `checklists`
				  LEFT JOIN `organizations` ON `checklists`.`organization_id` = `organizations`.`id`
				  LEFT JOIN `napravlenie_category` ON `checklists`.`napravlenie_parent_id` = `napravlenie_category`.`id`
				  LEFT JOIN `workers` as workers_1 ON `checklists`.`workers_1` = workers_1.`id`
				  LEFT JOIN `workers` as workers_2 ON `checklists`.`workers_2` = workers_2.`id`
				  LEFT JOIN `workers` as workers_3 ON `checklists`.`workers_3` = workers_3.`id`
				  WHERE `checklists`.`organization_id` IN (?a)
				  ORDER BY `organization_id` ?p";
		  
		  $checklists = $this->db->getAll($sql, $organization_available, $limit);
		  		  
		  return $checklists;				  
	  }
	 
	 function checklists_get_my($user_id, $show_all)
	  {
		  $limit = ($show_all)? '' : 'LIMIT 15';
		  
		  $sql = "SELECT `checklists`.*,
						 `organizations`.name as organization_name,
						 `napravlenie_category`.name as napravlenie_name,
						 CONCAT(workers_1.name, ' ', workers_1.surname) as worker_1_name,
						 CONCAT(workers_2.name, ' ', workers_2.surname) as worker_2_name,
						 CONCAT(workers_3.name, ' ', workers_3.surname) as worker_3_name
				  FROM `checklists`
				  LEFT JOIN `organizations` ON `checklists`.`organization_id` = `organizations`.`id`
				  LEFT JOIN `napravlenie_category` ON `checklists`.`napravlenie_parent_id` = `napravlenie_category`.`id`
				  LEFT JOIN `workers` as workers_1 ON `checklists`.`workers_1` = workers_1.`id`
				  LEFT JOIN `workers` as workers_2 ON `checklists`.`workers_2` = workers_2.`id`
				  LEFT JOIN `workers` as workers_3 ON `checklists`.`workers_3` = workers_3.`id`
				  WHERE `checklists`.`creator_id` = ?i
				  ORDER BY `organization_id` ?p";
		  
		  $checklists = $this->db->getAll($sql, $user_id, $limit);
		  		  
		  return $checklists;		  
	  }
	
	 function checklists_get_all($show_all)
	  {
		  $limit = ($show_all)? '' : 'LIMIT 15';		  
		  
		  $sql = "SELECT `checklists`.*,
						 `organizations`.name as organization_name,
						 `napravlenie_category`.name as napravlenie_name,
						 CONCAT(workers_1.name, ' ', workers_1.surname) as worker_1_name,
						 CONCAT(workers_2.name, ' ', workers_2.surname) as worker_2_name,
						 CONCAT(workers_3.name, ' ', workers_3.surname) as worker_3_name
				  FROM `checklists`
				  LEFT JOIN `organizations` ON `checklists`.`organization_id` = `organizations`.`id`
				  LEFT JOIN `napravlenie_category` ON `checklists`.`napravlenie_parent_id` = `napravlenie_category`.`id`
				  LEFT JOIN `workers` as workers_1 ON `checklists`.`workers_1` = workers_1.`id`
				  LEFT JOIN `workers` as workers_2 ON `checklists`.`workers_2` = workers_2.`id`
				  LEFT JOIN `workers` as workers_3 ON `checklists`.`workers_3` = workers_3.`id`
				  ORDER BY `organization_id` ?p";
		  
		  $checklists = $this->db->getAll($sql, $limit);
		  		  
		  return $checklists;
	  }
	  
	 function file_assign($list_id, $list_number, $file_name)
	  {
		  $scanlist_field = "scanlist_$list_number";
		  $sql 	   	      = "UPDATE `checklists` SET ?n = ?s WHERE `id` = ?i";
		  $this->db->query($sql, $scanlist_field, $file_name, $list_id);
	  }

	 function organization_get($organization_id)
	  {
		  return $this->db->getRow("SELECT * FROM `organizations` WHERE `id` = ?i", $organization_id);
	  }
	  
	 function napravlenie_search($search_term, $category_id/*, $search_inn*/)
	  {
		  /*
		  if($search_inn)
		   {
			  $search_query = $this->db->parse("`inn` = ?i", $search_term);
		   }
		  else
		   {		   
			  $search = "$search_term%";
			  $search_query = $this->db->parse("`name` LIKE ?s", $search);
		   }
		  */
		  
		  $search_term = "$search_term%";
		  $sql    = "SELECT id 	 as value,
							name as label
					 FROM `napravlenie` 
					 WHERE `category_id` = ?i AND (`name` LIKE ?s OR `inn` LIKE ?s)";
		  
		  // echo $this->db->parse($sql, $category_id, $search_term, $search_term);
		  
		  return $this->db->getAll($sql, $category_id, $search_term, $search_term);
	  }
	 
	 function list_sign($checklist_id, $checklist_number)
	  {

		  $title_list = $this->list_get($checklist_id);
		  
		  //print_r($title_list['subplace_type_id']);
		  
		  if(!empty($title_list['subplace_type_id']))
		   {
			  $work_type  = $this->work_type_build($title_list['subplace_type_id'])[0];
		   }
		  else
		   {
			  $work_type  = $this->work_type_build($title_list['place_type_id'])[0]; 
		   }
		  
		  foreach($work_type as $item)
		   {
			   if(isset($item['current'])) { $work_type = $item['name']; break; }
		   }
		  
		  /*
		  switch($work_type)
		   {
			   case 'Дезинсекция': $work_type = 'ДC'; break;
			   case 'Дезинфекция': $work_type = 'ДФ'; break;
			   case 'Дератизация': $work_type = 'ДР'; break;
			   case 'Акарицидная обработка': $work_type = 'АК'; break;			   
			   case 'Противомалярийная обработка': $work_type = 'МЛ'; break;			   
		   }
		  */
		  // print_r( $work_type );
		  // $shorters = $this->shorters;
		  $work_type = (isset($this->shorters[$work_type]))? $this->shorters[$work_type] : 'ERROR';
		  
		  
		  $date = $title_list['date_'.$checklist_number];
		  $date = str_replace('.','',$date);;
		  
		  // $list_per_day = sprintf("%03d", $this->lists_created_per_day());
		  $list_per_day = $this->lists_created_per_day();
		  
		  $list_code = $work_type . $title_list['organization_id'] . '_' . $title_list['brigada_'.$checklist_number] . '_' . $date . '_' . $list_per_day;
		  
		  $field = 'number_'.$checklist_number;
		  $sql   = "UPDATE `checklists` SET ?n = ?s WHERE `id` = ?i";
		  $this->db->query($sql, $field, $list_code, $checklist_id);
		  return $this->db->affectedRows();		  
	  }
	 
	 function lists_created_per_day()
	  {
		  $date = date('Y-m-d');
		  
		  $sql  = "SELECT COUNT(`id`)
				   FROM `checklists`
				   WHERE `date_1_real` = ?s";
		  $lists_per_day = $this->db->getOne($sql, $date);

		  $sql  = "SELECT COUNT(`id`)
				   FROM `checklists`
				   WHERE `date_2_real` = ?s";
		  $lists_per_day = $lists_per_day + $this->db->getOne($sql, $date);

		  $sql  = "SELECT COUNT(`id`)
				   FROM `checklists`
				   WHERE `date_3_real` = ?s";
		  $lists_per_day = $lists_per_day + $this->db->getOne($sql, $date);
		  
		  		  
		  $lists_per_day = ($lists_per_day > 0)? $lists_per_day : 1;
		  return $lists_per_day;
	  }
	 
	 function work_type_build($work_type_id)
	  {
		  // echo $work_type_id;
		  
		  // $work_type 	  = $this->work_type_get($work_type_id);
		  
		  // echo $work_type['parent_id'];
		  
		  $work_type_data   = $this->work_type_get_data($work_type_id);
		  $work_type_tree[] = $work_type_data['work_type_all'];
		  
		  while(!empty($work_type_data['work_type']['parent_id']))
		   {
			   $work_type_data   = $this->work_type_get_data($work_type_data['work_type']['parent_id']);
			   $work_type_tree[] = $work_type_data['work_type_all'];
		   }
		  
		  // print_r($work_type);
		  return array_reverse($work_type_tree);
	  }
	 
	 function work_type_get($work_type_id)
	  {
		  return $this->db->getRow("SELECT * FROM `work_types` WHERE `id` = ?i", $work_type_id);
	  }
	 
	 function napravlenie_build($napravlenie_id)
	  {
		  $napravlenie 		  = $this->napravlenie_get($napravlenie_id);
		  $napravlenie_data   = $this->napravlenie_get_data($napravlenie['category_id']);
		  $napravlenie_tree[] = $napravlenie_data['category_all'];
		  
		  while(!empty($napravlenie_data['category']['parent_id']))
		   {
			   $napravlenie_data   = $this->napravlenie_get_data($napravlenie_data['category']['parent_id']);
			   $napravlenie_tree[] = $napravlenie_data['category_all'];
		   }
		  
		  // print_r($napravlenie_tree); exit();
		  
		  return array_reverse($napravlenie_tree);
	  }
     
	 function work_type_get_all($category_id)
	  {
		  return $this->db->getAll("SELECT * FROM `work_types` WHERE `parent_id` = ?i", $category_id);
	  }
	 
	 function work_type_get_data($category_id)
	  {
		  $work_type 	 = $this->work_type_get($category_id);
		  $work_type_all = $this->work_type_get_all($work_type['parent_id']);
		  
		  foreach($work_type_all as &$item)
		   {
			   if($category_id == $item['id']) $item['current'] = 1;
		   }
		  
		  $work_type_tree = ['work_type' => $work_type, 'work_type_all' => $work_type_all];
		  
		  return $work_type_tree;
	  }
 
     function napravlenie_get_data($category_id)
	  {
		  // получаем категорию направления
		  $napravlenie_cat 	   = $this->napravlenie_cat_get($category_id);		 
		  $napravlenie_cat_all = $this->napravlenie_cat_get_all($napravlenie_cat['parent_id']);
		  foreach($napravlenie_cat_all as &$item)
		   {
			   if($item['id'] == $category_id) { $item['current'] = 1; }
			   $item['type'] = 'category';
		   }
		  
		  
		  // получаем все родительские направления
		  $napravlenie_tree = ['category' 	=> $napravlenie_cat,
							   'category_all' => $napravlenie_cat_all];	
		  return $napravlenie_tree;								 
	  }
	  
	 function napravlenie_cat_get($category_id)
	  {
		  return $this->db->getRow("SELECT * FROM `napravlenie_category` WHERE `id` = ?i", $category_id);
	  }
	 
	 function napravlenie_get($napravlenie_id)
	  {
		  $result = $this->db->getRow("SELECT * FROM `napravlenie` WHERE `id` = ?i", $napravlenie_id);
		  // print_r($result); exit();
		  return $result;
	  }
	 
	 function list_edit($data)
	  {
		   // проверяем ошибки
		   $errors = false;
		   if(empty($data['brigada']))
			{
				$errors['brigada'] = 'Не задана бригада';
			}
		   else
		    {
				if(empty($data['workers']))
				 {
					 $errors['workers'] = 'Не указаны работники';
				 }
			}
		   
		   if(empty($data['adress']['index']) ||
			  empty($data['adress']['city'])  ||
			  empty($data['adress']['street'])||
			  empty($data['adress']['rajon']) ||
			  empty($data['adress']['dom']))
			   {
				   if(empty($data['adress']['raw']))
					{
					   $errors['adress'] = 'Не задан адрес';
					}
				   else
					{
						$errors['adress'] = 'Адрес задан не полностью';
					}
			   }
		   
		   if(empty($data['napravlenie_id']))
			{
				$errors['napravlenie'] = 'Не задано направление';
			}
			
		   if(empty($data['subplace_type_id']) && $data['disabled_work_change'] == 0)
		    {
				$errors['work_type'] = 'Не задан тип работ';
			}
		
		   if(empty($data['question']) && $data['disabled_work_change'] == 0)
		    {
				$errors['question'] = 'Не задан опросник';
			}
		   
		   if(empty($data['date']))
		    {
				$errors['date'] = 'Не задана дата';
			}
		   
		   
		   if($errors)
		    {
				//print_r($errors);
				$result['status'] = 0;
				$result['errors'] = $errors;
				return $result;
			} 
		   
		   
		   
		   // создаём лист
		   // обработка переменных		   
		   $checklist_id	 = $data['checklist_id'];
		   $checklist_number = $data['checklist_number'];
		   $disabled_work_change = $data['disabled_work_change'];
		   
		   unset($data['disabled_work_change']);
		   
		   unset($data['checklist_id'], $data['checklist_number']);
		   
		   $data['date_'.$checklist_number] = date('Y-m-d', strtotime($data['date']));
		   unset($data['date']);
		   
		   unset($data['work_type'], $data['place_type']);
		   
		   $questions = $data['question'];
		   unset($data['question']);
			
			
		   $adress = $data['adress'];
		   unset($data['adress']);
		   
		   $data['workers_'.$checklist_number] = $data['workers'];
		   unset($data['workers']);
		   
		   $data['scanlist'.'_'.$checklist_number] = $data['scanlist'];
		   unset($data['scanlist']);
		 
           $data['brigada'.'_'.$checklist_number] = $data['brigada'];		 
		   unset($data['brigada']);
			
		   $date_real_field = 'date_'.$checklist_number.'_real';
			
		   $insert_data = $data;
		   
		   foreach($adress as $key=>$item)
		    {
				$insert_data['adress_' . $key] = $item;
			}
		   
		   $this->db->query("UPDATE `checklists` SET ?u, ?n = NOW() WHERE `id` = ?i", $insert_data, $date_real_field, $checklist_id);
		   
		   // сохраняем вопросы для листа 1
		   if($checklist_number == 1)
		    {		   
			   if($disabled_work_change == 0)
				{
				   $this->db->query("DELETE FROM `questions_data` WHERE `checklist_id` = ?i AND `checklist_number` = ?i", $checklist_id, $checklist_number);
				   
				   foreach($questions as $question_id=>$answer_number)
					{
						$data = ['checklist_id' 	=> $checklist_id,
								 'checklist_number' => $checklist_number,
								 'question_id'      => $question_id,
								 'answer_number'    => $answer_number];
						// echo $checklist_id; exit();
						// echo $this->db->parse("INSERT INTO `questions_data` SET ?u", $data);
						$this->db->query("INSERT INTO `questions_data` SET ?u", $data);
					}
				}
			}			
		   // сохраняем вопросы для второго листа	
		   elseif($checklist_number == 2)
		    {
				$this->db->query("DELETE FROM `questions_data` WHERE `checklist_id` = ?i AND `checklist_number` = ?i", $checklist_id, $checklist_number);
				
				// echo 'Сохраняем вопросы для второго листа';
				// print_r($questions);
				foreach($questions as $place_part_id=>$question_data_1)
				 {					 
					 foreach($question_data_1 as $work_type_id=>$question_data_2)
					  {
						  foreach($question_data_2 as $question_id=>$answer_number)
						   {
							   $insert_data = ['checklist_id' 	  => $checklist_id,
							                   'checklist_number' => $checklist_number,
											   'question_id'      => $question_id,
											   'place_part_id'    => $place_part_id,
											   'answer_number'    => $answer_number];
								
							   $this->db->query("INSERT INTO `questions_data` SET ?u", $insert_data);
						   }
					  }
				 }
			}
		   elseif($checklist_number == 3)
		    {
			   $this->db->query("DELETE FROM `questions_data` WHERE `checklist_id` = ?i AND `checklist_number` = ?i", $checklist_id, $checklist_number);
			   
			   foreach($questions as $place_part_id=>$question_data)
				{
					foreach($question_data as $question_id=>$answer_number)
					 {
						$data = ['checklist_id' 	=> $checklist_id,
								 'checklist_number' => $checklist_number,
								 'question_id'      => $question_id,
								 'place_part_id'    => $place_part_id,
								 'answer_number'    => $answer_number];
						// echo $checklist_id; exit();
						// echo $this->db->parse("INSERT INTO `questions_data` SET ?u", $data);
						
						$this->db->query("INSERT INTO `questions_data` SET ?u", $data);
					 }
				}
			}
			
			
		   $result['status']       = 1;
		   $result['checklist_id'] = $checklist_id;
		   return $result;
	  }

	 
	 function list_create($data)
	  {
		   // print_r($data);
		   
		   // проверяем ошибки
		   $errors = false;
		   if(empty($data['brigada']))
			{
				$errors['brigada'] = 'Не задана бригада';
			}
		   else
		    {
				if(empty($data['workers']))
				 {
					 $errors['workers'] = 'Не указаны работники';
				 }
			}
		   
		   if(empty($data['adress']['index']) ||
			  empty($data['adress']['city'])  ||
			  empty($data['adress']['street'])||
			  // empty($data['adress']['rajon']) ||
			  empty($data['adress']['dom']))
			   {
				   if(empty($data['adress']['raw']))
					{
					   $errors['adress'] = 'Не задан адрес';
					}
				   else
					{
						$errors['adress'] = 'Адрес задан не полностью';
					}
			   }
		   
		   if(empty($data['napravlenie_id']))
			{
				$errors['napravlenie'] = 'Не задано направление';
			}
			
		   if(empty($data['work_type']) || empty($data['place_type']))
		    {
				$errors['work_type'] = 'Не задан тип работ';
			}
		
		   if(empty($data['question']))
		    {
				$errors['question'] = 'Не задан опросник';
			}
		   
		   if(empty($data['date']))
		    {
				$errors['date'] = 'Не задана дата';
			}
		   
		   
		   if($errors)
		    {
				//print_r($errors);
				$result['status'] = 0;
				$result['errors'] = $errors;
				return $result;
			} 
		   
		   
		   
		   // создаём лист
		   // обработка переменных
		   $data['date_1'] = date('Y-m-d', strtotime($data['date'])); 
		   unset($data['date']);		   		   
		   unset($data['work_type']);
		   
		   $data['place_type_id'] = $data['place_type'];
		   unset($data['place_type']);		   
		   
		   $questions = $data['question'];
		   unset($data['question']);
		   
		   $adress = $data['adress'];
		   unset($data['adress']);
		   
		   $data['workers_1'] = $data['workers'];
		   unset($data['workers']);
		   
		   $data['scanlist_1'] = $data['scanlist'];
		   unset($data['scanlist']);
		   
		   $data['brigada_1'] = $data['brigada'];
		   unset($data['brigada']);	   		   
		   
		   $insert_data = $data;
		   
		   foreach($adress as $key=>$item)
		    {
				$insert_data['adress_' . $key] = $item;
			}
		   
		   $this->db->query("INSERT INTO `checklists` SET ?u, `date_1_real` = NOW()", $insert_data);
		   $checklist_id = $this->db->insertId();
		   
		   foreach($questions as $question_id=>$answer_number)
		    {
				$data = ['checklist_id' 	=> $checklist_id,
						 'checklist_number' => 1,
						 'question_id'      => $question_id,
						 'answer_number'    => $answer_number];
				$this->db->query("INSERT INTO `questions_data` SET ?u", $data);
			}
			
		   $result['status']       = 1;
		   $result['checklist_id'] = $checklist_id;
		   return $result;
	  }	 
	
     function list_get($list_id)
	  {
		 $sql = "SELECT list.*,
						DATE_FORMAT(list.date_1, '%d.%m.%Y') as date_1,
						DATE_FORMAT(list.date_2, '%d.%m.%Y') as date_2,
						DATE_FORMAT(list.date_3, '%d.%m.%Y') as date_3,
						org.name as organization
				 FROM `checklists` list
				 LEFT JOIN `organizations` org ON org.`id` = list.`organization_id`
				 WHERE list.`id` = ?i";
		 return $this->db->getRow($sql, $list_id);
	  }
	 
	 function workers_get($brigada_id)
	  {
		  return $this->db->getAll("SELECT * FROM `workers` WHERE `brigada_id` = ?i", $brigada_id);
	  }	 
	 
	 function answers_get($list_id, $list_number)
	  {
		  $sql = "SELECT answer.answer_number as answer_number,
		  				 answer.question_id   as question_id,
						 question.question    as question,
						 question.answer_1    as answer_1,
						 question.answer_2    as answer_2,
						 question.answer_3    as answer_3,
						 question.answer_4    as answer_4,
						 question.answer_5    as answer_5,
						 question.answer_6    as answer_6,
						 category_1.name      as category_3,
						 category_1.id        as category_3_id,
						 category_2.name      as category_2,
						 category_3.name      as category_1,
						 place_part.name      as place_part,
						 place_part.id        as place_part_id
				  FROM `questions_data` answer
				  LEFT JOIN `questions` question ON answer.`question_id` = question.`id`
				  LEFT JOIN `question_category` category_1 ON category_1.`id` = question.`category_id`
				  LEFT JOIN `question_category` category_2 ON category_2.`id` = category_1.`parent_id`
				  LEFT JOIN `question_category` category_3 ON category_3.`id` = category_2.`parent_id`
				  LEFT JOIN `question_category` place_part ON place_part.`id` = answer.`place_part_id`
				  WHERE `checklist_id` 	   = ?i AND
						`checklist_number` = ?i
				  ORDER BY category_1.name, category_2.name, category_3.name, answer.place_part_id, answer.question_id";
		  return $this->db->getAll($sql, $list_id, $list_number);
	  }
	 
	 function question_cat_get_by_name($name, $checklist_number)
	  {
		  $sql = "SELECT id
				  FROM `question_category`
				  WHERE `name`		 = ?s AND
						`check_list` = ?i";
		  // echo $this->db->parse($sql, $name, $checklist_number);
		  $result = $this->db->getOne($sql, $name, $checklist_number);
		  
		  return $result;		  
	  }
	 
	 function question_cats_get_by_id($category_id, $list_number)
	  {
		  // echo var_dump($category_id);
		  $sql = "SELECT *
				  FROM `question_category`
				  WHERE `parent_id` = ?i AND
						`check_list` = ?i";
          $result = $this->db->getAll($sql, $category_id, $list_number);						
		  // echo '321';
		  return $result;
	  }
	 
	 function questions_get($category_id, $check_list)
	  {
		  // echo $this->db->parse("SELECT * FROM `questions` WHERE `category_id` = ?i AND `check_list` = ?i", $category_id, $check_list);
		  return $this->db->getAll("SELECT * FROM `questions` WHERE `category_id` = ?i AND `check_list` = ?i", $category_id, $check_list);
	  }	 
	 
	 /*
	 function place_parts_get_id($subplace_type_id)
	  {
		  $sql = "SELECT *
				  FROM `question_category`
				  WHERE `id` = ?i";
		  $question_cat = $this->db->getRow($sql, $subplace_type_id);
		  print_r($question_cat);
	  }
	 */
	 
     function place_parts_get($work_type, $place_type, $check_list)
	  {
		  if($check_list == 1)
		   {
			  $sql = "SELECT id 
					  FROM `question_category`
					  WHERE `name` 	     = ?s AND
							`check_list` = ?i AND
							`parent_id` = (
											SELECT id 
											FROM `question_category` 
											WHERE `name` = ?s AND
												  `check_list` = ?i
										   )";
										   
			  $place_parts_parent_id = $this->db->getOne($sql, $place_type, $check_list, $work_type, $check_list);
		   }
		  else
		   {
					  $sql = "SELECT id 
							  FROM `question_category`
							  WHERE `name` 	     = ?s AND
									`check_list` = ?i";
                      $place_parts_parent_id = $this->db->getOne($sql, $work_type, $check_list);	
		   }
		  // echo $this->db->parse($sql, $place_type, $work_type); exit();
		  
		  // echo var_dump($place_parts_parent_id);
		  
		  if(!$place_parts_parent_id)
		   {
			   return array();
		   }
		  
		  // echo $place_parts_parent_id; exit();
		  
		  $place_parts = $this->db->getAll("SELECT * FROM `question_category` WHERE `parent_id` = ?s", $place_parts_parent_id);
		  
		  // print_r($place_parts); exit(); 
		  
		  return $place_parts;
	  }
	  
	 function work_types_get($parent_id=false)
	  {
		  if(!$parent_id) $parent_id = 0;
		  return $this->db->getAll("SELECT * FROM `work_types` WHERE `parent_id` = ?i", $parent_id);
	  }
	 
	 function napravlenie_cat_get_all($parent_id=false)
	  { 
		  if(!$parent_id) $parent_id = 0;
		  return $this->db->getAll("SELECT * FROM `napravlenie_category` WHERE `parent_id` = ?i", $parent_id);
	  }
	  
	 /*
	 function napravlenie_and_cat_get($parent_id=false)
	  {
		  $result['category']    = $this->napravlenie_cat_get_all($parent_id);
		  $result['napravlenie'] = $this->napravlenie_get_all($parent_id);
		  return $result;
	  }
	 
	 function napravlenie_get_all($parent_id=false)
	  {
		  if(!$parent_id) $parent_id = 0;
		  return $this->db->getAll("SELECT *, 'napravlenie' as type FROM `napravlenie` WHERE `category_id` = ?i", $parent_id);
	  }

	 function napravlenie_cat_get_all($parent_id=false)
	  { 
		  if(!$parent_id) $parent_id = 0;
		  return $this->db->getAll("SELECT *, 'category' as type FROM `napravlenie_category` WHERE `parent_id` = ?i", $parent_id);
	  }
	 */
	 
	 function user_data_get($user_id)
	  {
		  return $this->db->getRow("SELECT * FROM `users` WHERE `id` = ?i", $user_id);
	  }
	 
	 function brigadu_get($organization_id)
	  {
		  return $this->db->getAll("SELECT * FROM `brigadu` WHERE `organization_id` = ?i", $organization_id);
	  }
	 
	 function organization_data_get($organization_id)
	  {
		  return $this->db->getRow("SELECT * FROM `organizations` WHERE `id` = ?i", $organization_id);
	  }	 
	 
	 function check_auth($login, $password)
	  {
		  $sql = "SELECT * FROM `users` WHERE `login` = ?s AND `password` = MD5(?s)";
		  $result = $this->db->getRow($sql, $login, $password);
		  return (!is_null($result))? $result : false;
	  }	 
	  
 }
?>