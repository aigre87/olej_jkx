<?php
header ("Content-Type:text/html");
if ($_GET['tab'] == '43gdg6') {

    include "../config.php";
    $mysql = $config['mysql'];

// коннектимся к базе данных
    $mysql_database 	= $mysql['db']; //Имя базы данных
    $mysql_username 	= $mysql['user']; //Имя пользователя базы данных
    $mysql_password 	= $mysql['pass']; //Пароль пользователя базы данных
    $mysql_host 		= $mysql['host']; //Сервер базы данных
    $dbpf 				= ''; //Префикс таблиц в базе данных
    //Соединяемся с базой данных
    $mysql_connect = mysql_connect($mysql_host, $mysql_username, $mysql_password) or die("нет");
    //Выбираем базу данных для работы
    mysql_select_db($mysql_database);
    //Устанавливаем кодировку для соединения базы данных
    mysql_query("SET NAMES utf8");

    require_once "Classes/PHPExcel.php";
    $phpexcel = new PHPExcel();

    // фон работ
    function worksbg($work) {
        $bg = array(
            'Помещения'				=> 'bdefff',
            'Помещение'				=> 'cbbdff',
            'Фундамент'				=> 'ffbde1',
            'Газоны(кустарники)'	=> 'ffebbd',
            'Газоны'				=> 'd3ffbd',
            'Мусоростволы'			=> 'eaadad',
            'Контейнеры для мусора'	=> 'eae0ad',
            'Мусорная камера'		=> 'adeab5',
            'Мусорные камеры'		=> 'addeea',
            'Контейнерные площадки'	=> 'adbaea',
            'Лестничные площадки'	=> 'cbadea',
            'Чердак'				=> 'eaadad',
            'Чердаки'				=> 'b4bf7b',
            'Подвалы'				=> '7ba5bf',
            'Подвал'				=> '7b89bf'
        );
        return strtr($work,$bg);
    }




    ?>

    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Таблицы с данными</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <h2><a href="tables.xlsx" class="btn btn-default">Скачать</a></h2>
    <table class="table table-bordered" >
        <thead>
        <tr>
            <th>#</th>
            <th>Ораганизация</th>
            <th>Адрес индекс</th>
            <th>Адрес Город</th>
            <th>Адрес улица</th>
            <th>Адрес район</th>
            <th>Адрес округ</th>
            <th>Адрес дом</th>
            <th>Адрес корпус(стр. и т.п.)</th>
            <th>Адрес квартира (офис)</th>
            <th>Направление(жкх, образование)</th>
            <th>Направление(тип организации)</th>
            <th>Направление(организация) </th>
            <th>Вид работы (Дезинсекция, дератизация и т.д.)</th>
            <th>Вид работы место (Территория, здание,водоемы)</th>
            <th>Вид работы Место тип здания</th>
            <th>Статус</th>
            <th>Номер листа</th>
            <th>Бригада</th>
            <th>Работник</th>
            <th>Файл</th>
            <th>Дата</th>
            <th>Место работы</th>
            <th>Данные работы</th>
            <th>Оценка</th>
            <th style="background: #ff0000;">Способ обработки</th>


            <?php
            $page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
            $columnPosition = 0; // Начальная координата x
            $startLine = 1; // Начальная координата y


            $columns_t = ['№', 'Ораганизация', 'Адрес индекс', 'Адрес Город', 'Адрес улица', '	Адрес район', 'Адрес округ', 'Адрес дом', 'Адрес корпус(стр. и т.п.)', 'Адрес квартира (офис)', 'Направление(жкх, образование)', 'Направление(тип организации)', 'Направление(организация', 'Вид работы (Дезинсекция, дератизация и т.д.)', 'Вид работы место (Территория, здание,водоемы)', 'Вид работы Место тип здания', 'Статус', 'Номер листа', 'Бригада', 'Работник', 'Файл', 'Дата', 'Место работы', 'Данные работы', 'Оценка'];
            // Указатель на первый столбец
            $currentColumn = $columnPosition;
            // Формируем шапку
            foreach ($columns_t as $column) {
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $column);
                $currentColumn++; // Смещаемся вправо
            }
            $page->getStyleByColumnAndRow($currentColumn, $startLine)
                ->getFill()
                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('ff0000');
            $page->setCellValueByColumnAndRow($currentColumn, $startLine, 'Способ обработки');


            // Перекидываем указатель на следующую строку
            $startLine++;

            ?>

        </tr>
        </thead>
        <tbody>
        <?php


        $number_color = 0;
        $sql = "SELECT 
chl.id AS id, 
chl.number_1 AS number_1, 
chl.adress_index AS adress_index, 
chl.adress_city AS adress_city, 
chl.adress_street AS adress_street, 
chl.adress_rajon AS adress_rajon, 
chl.adress_okrug AS adress_okrug, 
chl.adress_dom AS adress_dom, 
chl.adress_block AS adress_block, 
chl.adress_appartment AS adress_appartment, 
chl.number_1 AS number_1, 
chl.brigada_1 AS brigada_1, 
chl.date_1 AS date_1, 
chl.number_2 AS number_2, 
chl.brigada_2 AS brigada_2, 
chl.date_2 AS date_2, 
chl.number_3 AS number_3, 
chl.brigada_3 AS brigada_3, 
chl.date_3 AS date_3, 
n.name AS n_name, 
n2.name AS n2_name, 
np.name AS np_name, 
wt1.name AS workers_1, 
wt1.middlename AS middlename_1, 
wt1.surname AS surname_1, 
wt2.name AS workers_2, 
wt2.middlename AS middlename_2, 
wt2.surname AS surname_2, 
wt3.name AS workers_3, 
wt3.middlename AS middlename_3, 
wt3.surname AS surname_3, 
w1.name AS w1_name, 
w2.name AS w2_name, 
w3.name AS w3_name, 
org.name AS org_name 
FROM `checklists` chl 
LEFT JOIN `organizations` org ON (org.id = chl.organization_id) 
LEFT JOIN `napravlenie_category` n ON (n.id = chl.napravlenie_parent_id) 
LEFT JOIN `napravlenie` np ON (np.id = chl.napravlenie_id) 
LEFT JOIN `napravlenie_category` n2 ON (n2.id = np.category_id) 
LEFT JOIN `workers` wt1 ON (wt1.id = chl.workers_1) 
LEFT JOIN `workers` wt2 ON (wt2.id = chl.workers_2) 
LEFT JOIN `workers` wt3 ON (wt3.id = chl.workers_3) 
LEFT JOIN `work_types` w1 ON (w1.id = chl.place_type_id) 
LEFT JOIN `work_types` w2 ON (w2.id = chl.subplace_type_id) 
LEFT JOIN `work_types` w3 ON (w3.id = w1.parent_id) 
GROUP BY chl.id";

        $result = mysql_query($sql);

        while ($row = mysql_fetch_assoc($result)) {

            $pos_name = $row['org_name'];
            $row['adress_index'];
            if ($row['date_1'] != '0000-00-00') {
                $date_1 = $row['date_1'];
            } else {
                $date_1 = '-';
            }
            if ($row['date_2'] != '0000-00-00') {
                $date_2 = $row['date_2'];

            } else {
                $date_2 = '-';
            }
            if ($row['date_3'] != '0000-00-00') {
                $date_3 = $row['date_3'];
            } else {
                $date_3 = '-';
            }


            $number_color++;
           $background =  ($number_color % 2) ? "#f1f2f2;" : "#fff;";


            $sql1 = "SELECT answer.answer_number as answer_number,
		  				 answer.question_id   as question_id,
						 question.question    as question,
						 question.answer_1    as answer_1,
						 question.answer_2    as answer_2,
						 question.answer_3    as answer_3,
						 question.answer_4    as answer_4,
						 question.answer_5    as answer_5,
						 question.answer_6    as answer_6,
						 category_1.name      as category_3,
						 category_1.id        as category_3_id,
						 category_2.name      as category_2,
						 category_3.name      as category_1,
						 place_part.name      as place_part,
						 place_part.id        as place_part_id
				  FROM `questions_data` answer
				  LEFT JOIN `questions` question ON answer.`question_id` = question.`id`
				  LEFT JOIN `question_category` category_1 ON category_1.`id` = question.`category_id`
				  LEFT JOIN `question_category` category_2 ON category_2.`id` = category_1.`parent_id`
				  LEFT JOIN `question_category` category_3 ON category_3.`id` = category_2.`parent_id`
				  LEFT JOIN `question_category` place_part ON place_part.`id` = answer.`place_part_id`
				  WHERE `checklist_id` = '" . $row['id'] . "' AND `checklist_number` = '1'
				  ORDER BY category_1.name, category_2.name, category_3.name, answer.place_part_id, answer.question_id";
            $result1 = mysql_query($sql1);
            $row_col1_111 = 0;
            $row2_col = 0;
            while ($question = mysql_fetch_assoc($result1)) {
                $currentColumn = 0;
                echo '<tr style="background: ' . $background . ' " >';
                echo '<td> ' . $row['id'] . ' </td>';
                echo '<td> ' . $pos_name . ' </td>';
                echo '<td> ' . $row['adress_index'] . ' </td>';
                echo '<td> ' . $row['adress_city'] . ' </td>';
                echo '<td> ' . $row['adress_street'] . ' </td>';
                echo '<td> ' . $row['adress_rajon'] . ' </td>';
                echo '<td> ' . $row['adress_okrug'] . ' </td>';
                echo '<td> ' . $row['adress_dom'] . ' </td>';
                echo '<td> ' . $row['adress_block'] . ' </td>';
                echo '<td> ' . $row['adress_appartment'] . ' </td>';
                echo '<td> ' . $row['n_name'] . ' </td>';
                echo '<td> ' . $row['n2_name'] . ' </td>';
                echo '<td> ' . $row['np_name'] . ' </td>';
                echo '<td> ' . $row['w3_name'] . ' </td>';
                echo '<td> ' . $row['w1_name'] . ' </td>';
                echo '<td> ' . $row['w2_name'] . ' </td>';
                echo '<td style="background: #ff9435;">Обследование</td>';
                echo '<td style="background: #ff9435;"> ' . $row['number_1'] . ' </td>';
                echo '<td style="background: #ff9435;"> ' . $row['brigada_1'] . ' </td>';
                echo '<td style="background: #ff9435;"> ' . $row['workers_1'] . ' ' . $row['middlename_1'] . ' ' . $row['surname_1'] . ' </td>';
                echo '<td style="background: #ff9435;"> ' . $row[''] . ' </td>';
                echo '<td style="background: #ff9435;"> ' . $date_1 . ' </td>';
                $question_category = $question['category_3'];

                $setRGB = worksbg($question_category);
                if ($setRGB == '') {
                    $setRGB = 'ffffff';
                }

                echo '<td style="background: #'.$setRGB.';">' .  $question_category . ' </td>';
                echo '<td style="background: #'.$setRGB.';"> ' .$question['question'] . ' </td>';

                $workers_1 = $row['workers_1'] . ' ' . $row['middlename_1'] . ' ' . $row['surname_1'];

                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['id']);
                // Смещаемся вправо
                $currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $pos_name);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_index']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_city']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_street']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_rajon']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_okrug']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_dom']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_block']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_appartment']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n2_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['np_name'] );$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w3_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w1_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w2_name']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, 'Обследование');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['number_1']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['brigada_1']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $workers_1);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, '');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff9435');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $date_1);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question_category);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['question']);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                if ($question['answer_number'] == 1) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_1'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_1']);$currentColumn++;
                } elseif ($question['answer_number'] == 2) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_2'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_2']);$currentColumn++;
                } elseif ($question['answer_number'] == 3) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_3'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_3']);$currentColumn++;
                } elseif ($question['answer_number'] == 4) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_4'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_4']);$currentColumn++;
                } elseif ($question['answer_number'] == 5) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_5'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_5']);$currentColumn++;
                } elseif ($question['answer_number'] == 6) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_6'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_6']);$currentColumn++;
                }
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff0000');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['category_1']);
                // Перекидываем указатель на следующую строку
                $startLine++;

                echo '<td style="background: #ff0000;">' . $question['category_1'] . '</td>';
                echo '</tr>';




            }


            $sql1 = "SELECT answer.answer_number as answer_number,
		  				 answer.question_id   as question_id,
						 question.question    as question,
						 question.answer_1    as answer_1,
						 question.answer_2    as answer_2,
						 question.answer_3    as answer_3,
						 question.answer_4    as answer_4,
						 question.answer_5    as answer_5,
						 question.answer_6    as answer_6,
						 category_1.name      as category_3,
						 category_1.id        as category_3_id,
						 category_2.name      as category_2,
						 category_3.name      as category_1,
						 place_part.name      as place_part,
						 place_part.id        as place_part_id
				  FROM `questions_data` answer
				  LEFT JOIN `questions` question ON answer.`question_id` = question.`id`
				  LEFT JOIN `question_category` category_1 ON category_1.`id` = question.`category_id`
				  LEFT JOIN `question_category` category_2 ON category_2.`id` = category_1.`parent_id`
				  LEFT JOIN `question_category` category_3 ON category_3.`id` = category_2.`parent_id`
				  LEFT JOIN `question_category` place_part ON place_part.`id` = answer.`place_part_id`
				  WHERE `checklist_id` = '" . $row['id'] . "' AND `checklist_number` = '2'
				  ORDER BY category_1.name, category_2.name, category_3.name, answer.place_part_id, answer.question_id";
            $result1 = mysql_query($sql1);
            $row_col2_111 = 0;
            $row22_col = 0;
            $question_category = '';
            while ($question = mysql_fetch_assoc($result1)) {
                $currentColumn = 0;

                echo '<tr style="background: ' . $background . ' " >';
                echo '<td> ' . $row['id'] . ' </td>';
                echo '<td> ' . $pos_name . ' </td>';
                echo '<td> ' . $row['adress_index'] . ' </td>';
                echo '<td> ' . $row['adress_city'] . ' </td>';
                echo '<td> ' . $row['adress_street'] . ' </td>';
                echo '<td> ' . $row['adress_rajon'] . ' </td>';
                echo '<td> ' . $row['adress_okrug'] . ' </td>';
                echo '<td> ' . $row['adress_dom'] . ' </td>';
                echo '<td> ' . $row['adress_block'] . ' </td>';
                echo '<td> ' . $row['adress_appartment'] . ' </td>';
                echo '<td> ' . $row['n_name'] . ' </td>';
                echo '<td> ' . $row['n2_name'] . ' </td>';
                echo '<td> ' . $row['np_name'] . ' </td>';
                echo '<td> ' . $row['w3_name'] . ' </td>';
                echo '<td> ' . $row['w1_name'] . ' </td>';
                echo '<td> ' . $row['w2_name'] . ' </td>';
                echo '<td style="background: #73dc72;">Истребление</td>';
                echo '<td style="background: #73dc72;"> ' . $row['number_2'] . ' </td>';
                echo '<td style="background: #73dc72;"> ' . $row['brigada_2'] . ' </td>';
                echo '<td style="background: #73dc72;"> ' . $row['workers_2'] . ' ' . $row['middlename_2'] . ' ' . $row['surname_2'] . ' </td>';
                $workers_2 = $row['workers_2'] . ' ' . $row['middlename_2'] . ' ' . $row['surname_2'];
                echo '<td style="background: #73dc72;"> ' . $row[''] . ' </td>';
                echo '<td style="background: #73dc72;"> ' . $date_2 . ' </td>';
                $question_category = $question['place_part'];
                $setRGB = worksbg($question_category);
                if ($setRGB == '') {
                    $setRGB = 'ffffff';
                }
                echo '<td style="background: #'.$setRGB.';">' .  $question_category . ' </td>';
                echo '<td style="background: #'.$setRGB.';"> ' .$question['question'] . ' </td>';

                $workers_2 = $row['workers_2'] . ' ' . $row['middlename_2'] . ' ' . $row['surname_2'];

                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['id']);
                // Смещаемся вправо
                $currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $pos_name);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_index']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_city']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_street']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_rajon']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_okrug']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_dom']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_block']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_appartment']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n2_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['np_name'] );$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w3_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w1_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w2_name']);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, 'Истребление');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['number_2']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['brigada_2']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $workers_2);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, '');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('73dc72');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $date_2);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question_category);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['question']);$currentColumn++;


                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                if ($question['answer_number'] == 1) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_1'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_1']);$currentColumn++;
                } elseif ($question['answer_number'] == 2) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_2'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_2']);$currentColumn++;
                } elseif ($question['answer_number'] == 3) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_3'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_3']);$currentColumn++;
                } elseif ($question['answer_number'] == 4) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_4'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_4']);$currentColumn++;
                } elseif ($question['answer_number'] == 5) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_5'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_5']);$currentColumn++;
                } elseif ($question['answer_number'] == 6) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_6'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_6']);$currentColumn++;
                }

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff0000');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['category_3']);
                // Перекидываем указатель на следующую строку
                $startLine++;
                echo '<td style="background: #ff0000;">' . $question['category_3'] . '</td>';
                echo '</tr>';
            }


            $sql1 = "SELECT answer.answer_number as answer_number,
		  				 answer.question_id   as question_id,
						 question.question    as question,
						 question.answer_1    as answer_1,
						 question.answer_2    as answer_2,
						 question.answer_3    as answer_3,
						 question.answer_4    as answer_4,
						 question.answer_5    as answer_5,
						 question.answer_6    as answer_6,
						 category_1.name      as category_3,
						 category_1.id        as category_3_id,
						 category_2.name      as category_2,
						 category_3.name      as category_1,
						 place_part.name      as place_part,
						 place_part.id        as place_part_id
				  FROM `questions_data` answer
				  LEFT JOIN `questions` question ON answer.`question_id` = question.`id`
				  LEFT JOIN `question_category` category_1 ON category_1.`id` = question.`category_id`
				  LEFT JOIN `question_category` category_2 ON category_2.`id` = category_1.`parent_id`
				  LEFT JOIN `question_category` category_3 ON category_3.`id` = category_2.`parent_id`
				  LEFT JOIN `question_category` place_part ON place_part.`id` = answer.`place_part_id`
				  WHERE `checklist_id` = '" . $row['id'] . "' AND `checklist_number` = '3'
				  ORDER BY category_1.name, category_2.name, category_3.name, answer.place_part_id, answer.question_id";
            $result1 = mysql_query($sql1);
            $row_col3_111 = 0;
            $row23_col = 0;
            $question_category = '';
            while ($question = mysql_fetch_assoc($result1)) {
                $currentColumn = 0;
                echo '<tr style="background: ' . $background . ' " >';
                echo '<td> ' . $row['id'] . ' </td>';
                echo '<td> ' . $pos_name . ' </td>';
                echo '<td> ' . $row['adress_index'] . ' </td>';
                echo '<td> ' . $row['adress_city'] . ' </td>';
                echo '<td> ' . $row['adress_street'] . ' </td>';
                echo '<td> ' . $row['adress_rajon'] . ' </td>';
                echo '<td> ' . $row['adress_okrug'] . ' </td>';
                echo '<td> ' . $row['adress_dom'] . ' </td>';
                echo '<td> ' . $row['adress_block'] . ' </td>';
                echo '<td> ' . $row['adress_appartment'] . ' </td>';
                echo '<td> ' . $row['n_name'] . ' </td>';
                echo '<td> ' . $row['n2_name'] . ' </td>';
                echo '<td> ' . $row['np_name'] . ' </td>';
                echo '<td> ' . $row['w3_name'] . ' </td>';
                echo '<td> ' . $row['w1_name'] . ' </td>';
                echo '<td> ' . $row['w2_name'] . ' </td>';
                echo '<td style="background: #40b7ff;">Результат</td>';
                echo '<td style="background: #40b7ff;"> ' . $row['number_3'] . ' </td>';
                echo '<td style="background: #40b7ff;"> ' . $row['brigada_3'] . ' </td>';
                echo '<td style="background: #40b7ff;"> ' . $row['workers_3'] . ' ' . $row['middlename_3'] . ' ' . $row['surname_3'] . ' </td>';
                $workers_3 = $row['workers_3'] . ' ' . $row['middlename_3'] . ' ' . $row['surname_3'];
                echo '<td style="background: #40b7ff;"> ' . $row[''] . ' </td>';
                echo '<td style="background: #40b7ff;"> ' . $date_3 . ' </td>';

                $question_category = $question['place_part'];
                $setRGB = worksbg($question_category);
                if ($setRGB == '') {
                    $setRGB = 'ffffff';
                }
                echo '<td style="background: #'.$setRGB.';">' .  $question_category . ' </td>';
                echo '<td style="background: #'.$setRGB.';"> ' .$question['question'] . ' </td>';
                $workers_3 = $row['workers_3'] . ' ' . $row['middlename_3'] . ' ' . $row['surname_3'];

                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['id']);
                // Смещаемся вправо
                $currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $pos_name);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_index']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_city']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_street']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_rajon']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_okrug']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_dom']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_block']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['adress_appartment']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['n2_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['np_name'] );$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w3_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w1_name']);$currentColumn++;
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['w2_name']);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, 'Результат');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['number_3']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $row['brigada_3']);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $workers_3);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, '');$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('40b7ff');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $date_3);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question_category);$currentColumn++;
                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['question']);$currentColumn++;

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB($setRGB);
                if ($question['answer_number'] == 1) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_1'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_1']);$currentColumn++;
                } elseif ($question['answer_number'] == 2) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_2'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_2']);$currentColumn++;
                } elseif ($question['answer_number'] == 3) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_3'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_3']);$currentColumn++;
                } elseif ($question['answer_number'] == 4) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_4'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_4']);$currentColumn++;
                } elseif ($question['answer_number'] == 5) {
                    echo '<td style="background: #'.$setRGB.';"> ' . $question['answer_5'] . ' </td>';
                    $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['answer_5']);$currentColumn++;
                }

                $page->getStyleByColumnAndRow($currentColumn, $startLine)
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('ff0000');
                $page->setCellValueByColumnAndRow($currentColumn, $startLine, $question['category_3']);
                // Перекидываем указатель на следующую строку
                $startLine++;
                echo '<td style="background: #ff0000;">' . $question['category_3'] . '</td>';
                echo '</tr>';
            }

        }
        ?>
        </tbody>
    </table>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    </body>
    </html>

<?php
    /* Начинаем готовиться к записи информации в xlsx-файл */
    $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
    $objWriter->save("tables.xlsx");
    /* Записываем в файл */
?>
<?php

}
?>