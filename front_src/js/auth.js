(function() {
	function loginForm(){
		var $form = $(".loginForm"),
			$login = $form.find("input[name='login']"),
			$pass = $form.find("input[name='password']");

		function validateName(){
			if ( $login.val().trim().length > 0 ){
				$login.addClass("complete").removeClass("uncomplete");
			}else{
				$login.removeClass("complete").addClass("uncomplete");
			}
		}
		function validatePassword(){
			if ( $pass.val().trim().length > 0 ){
				$pass.addClass("complete").removeClass("uncomplete");;
			}else{
				$pass.removeClass("complete").addClass("uncomplete");;
			}
		}

		$form.on("submit", function(e){
			validateName();
			validatePassword();
			if( $form.find(".uncomplete").length > 0 ){
	            $.magnificPopup.open({
	              items: {
	                src: "<div class='defaultPopupContent mfp-with-anim'>Заполните необходимые поля</div>",
	                type: 'inline'
	              },
	              removalDelay: 500, //delay removal by X to allow out-animation
	              closeBtnInside: true,
	              mainClass: 'mfp-with-zoom',
	              callbacks: {
	                beforeOpen: function() {
	                  this.st.mainClass = "mfp-zoom-in defaultPopup";
	                },
	                beforeClose: function() {

	                },
	              },
	              midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
	            });
	            return false;
			}
		});
	}

    $(document).ready(function() {
		if( $(".loginForm").length > 0 ){
			loginForm();
		}
    });
})();
